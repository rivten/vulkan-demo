
#define TINYOBJ_LOADER_C_IMPLEMENTATION
#include "tiny_obj_loader.h"

enum vertex_layout_type
{
	VertexLayoutType_PPP_NNN_UV,
};

struct vertex
{
	v3 P;
	v3 N;
	v2 UV;
};

global_variable v3 Offset = V3(0.2f, 0.0f, -0.5f);
global_variable vertex Vertices[] =
{
	{V3(-0.5f, -0.5f, 0.0f), V3(1.0f, 0.0f, 1.0f), V2(0.0f, 1.0f)},
	{V3(-0.5f, 0.5f, 0.0f), V3(1.0f, 0.0f, 0.0f), V2(0.0f, 0.0f)},
	{V3(0.5f, -0.5f, 0.0f), V3(0.0f, 1.0f, 0.0f), V2(1.0f, 1.0f)},
	{V3(0.5f, 0.5f, 0.0f), V3(0.0f, 0.0f, 1.0f), V2(1.0f, 0.0f)},

	{V3(-0.5f, -0.5f, 0.0f) + Offset, V3(1.0f, 0.0f, 1.0f), V2(0.0f, 1.0f)},
	{V3(-0.5f, 0.5f, 0.0f) + Offset, V3(1.0f, 0.0f, 0.0f), V2(0.0f, 0.0f)},
	{V3(0.5f, -0.5f, 0.0f) + Offset, V3(0.0f, 1.0f, 0.0f), V2(1.0f, 1.0f)},
	{V3(0.5f, 0.5f, 0.0f) + Offset, V3(0.0f, 0.0f, 1.0f), V2(1.0f, 0.0f)},
};

global_variable u32 Indices[] = {0, 1, 2, 3, 2, 1,  4, 5, 6, 7, 6, 5};

enum texture_storage_type
{
	TextureStorageType_None,
	TextureStorageType_TextureData,
	TextureStorageType_TextureID,

	TextureStorageType_Count,
};

typedef u32 triangle[3];
struct mesh
{
	u32 VertexCount;
	vertex* Vertices;

	u32 TriangleCount;
	triangle* Triangles;

	u32 TextureID;
};

struct texture_list
{
	u32 Count;
	bitmap Textures[64];
};

struct mesh_list
{
	u32 Count;
	mesh Meshes[512];
};

internal mesh
LoadTeapot(void)
{
	FILE* ObjFileHandle = fopen("untitled.obj", "r");
	Assert(ObjFileHandle);
	size_t FileSize = GetFileSizeInBytes(ObjFileHandle);
	char* Buffer = AllocateArray(char, FileSize);
	fread(Buffer, FileSize, 1, ObjFileHandle);

	tinyobj_attrib_t Attributes[16] = {};
	tinyobj_shape_t* Shapes[16] = {};
	size_t ShapeCount = 0;
	tinyobj_material_t* Materials[16] = {};
	size_t MaterialCount = 0;

	int ObjLoadResult = tinyobj_parse_obj(Attributes,
			Shapes, &ShapeCount, Materials, &MaterialCount,
			Buffer, FileSize, 0);
	Assert(ObjLoadResult == TINYOBJ_SUCCESS);
	Free(Buffer);

	mesh Result = {};

	Result.VertexCount = Attributes[0].num_vertices;
	Result.Vertices = AllocateArray(vertex, Result.VertexCount);
	for(u32 VertexIndex = 0; VertexIndex < Result.VertexCount; ++VertexIndex)
	{
		float* v = Attributes[0].vertices + 3 * VertexIndex;
		v3 P = V3(v[0], v[1], v[2]);
		Result.Vertices[VertexIndex] = {P, V3(0.0f, 0.0f, 0.0f), V2(0.0f, 0.0f)};
	}
	Result.TriangleCount = Attributes[0].num_face_num_verts;
	Result.Triangles = AllocateArray(triangle, Result.TriangleCount);
	for(u32 TriangleIndex = 0; TriangleIndex < Result.TriangleCount; ++TriangleIndex)
	{
		tinyobj_vertex_index_t* VertexIndex = Attributes[0].faces + 3 * TriangleIndex;
		Result.Triangles[TriangleIndex][0] = VertexIndex[0].v_idx;
		Result.Triangles[TriangleIndex][1] = VertexIndex[1].v_idx;
		Result.Triangles[TriangleIndex][2] = VertexIndex[2].v_idx;
	}

	return(Result);
}

struct msh_vertex
{
	v3 P;
	v3 N;
	v2 UV;
	v3 T;
	v3 B;
};

internal void
DebugPrintVertex(vertex V)
{
	printf("P = (%f, %f, %f)\n", V.P.x, V.P.y, V.P.z);
	printf("N = (%f, %f, %f)\n", V.N.x, V.N.y, V.N.z);
	printf("UV = (%f, %f)\n", V.UV.x, V.UV.y);
}

internal void
DebugPrintTriangle(triangle T)
{
	printf("Triangle = (%i, %i, %i)\n", T[0], T[1], T[2]);
}

// TODO(hugo): Not thrilled about the half-functionnal half-procedural
// aspect of this function
internal mesh LoadMSHFromFile(FILE* FileHandle, texture_list* TextureList)
{
	mesh Result = {};

	fread(&Result.VertexCount, sizeof(u32), 1, FileHandle);
	Result.Vertices = AllocateArray(vertex, Result.VertexCount);
	for(u32 VertexIndex = 0; VertexIndex < Result.VertexCount; ++VertexIndex)
	{
		msh_vertex V = {};
		fread(&V, sizeof(msh_vertex), 1, FileHandle);
		Result.Vertices[VertexIndex] = {V.P, V.N, V.UV};
	}

	fread(&Result.TriangleCount, sizeof(u32), 1, FileHandle);
	Result.Triangles = AllocateArray(triangle, Result.TriangleCount);
	fread(Result.Triangles, sizeof(triangle), Result.TriangleCount, FileHandle);

	texture_storage_type TextureStorageType = TextureStorageType_None;
	fread(&TextureStorageType, sizeof(texture_storage_type), 1, FileHandle);
	switch(TextureStorageType)
	{
		case TextureStorageType_None :
			{
				// TODO(hugo): Do something to properly handle meshes with
				// no textures
				Result.TextureID = 0;
			} break;
		case TextureStorageType_TextureData :
			{
				bitmap Texture = {};
				fread(&Texture.Width, sizeof(s32), 1, FileHandle);
				Assert(Texture.Width > 0);
				fread(&Texture.Height, sizeof(s32), 1, FileHandle);
				Assert(Texture.Height > 0);
				fread(&Texture.BytesPerPixels, sizeof(u32), 1, FileHandle);
				Assert(Texture.BytesPerPixels == 3);
				// TODO(hugo): Stop forcing 4 bps at some point
				Texture.BytesPerPixels = 4;
				u32 PixelCount = Texture.Width * Texture.Height;
				u32 TextureSizeInBytes = PixelCount * Texture.BytesPerPixels;
				Texture.Data = AllocateArray(u8, TextureSizeInBytes);
				for(u32 PixelIndex = 0; PixelIndex < PixelCount; ++PixelIndex)
				{
					u8* Pixel = (u8*)((u32*)Texture.Data + PixelIndex);
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					*Pixel = 255;
				}

				Assert(TextureList->Count < ArrayCount(TextureList->Textures));
				TextureList->Textures[TextureList->Count] = Texture;
				Result.TextureID = TextureList->Count;

				++TextureList->Count;
			} break;
		case TextureStorageType_TextureID :
			{
				fread(&Result.TextureID, sizeof(u32), 1, FileHandle);
			} break;
		InvalidDefaultCase;
	};

	texture_storage_type UnreadTextureType = TextureStorageType_None;
	fread(&UnreadTextureType, sizeof(texture_storage_type), 1, FileHandle);
	switch(UnreadTextureType)
	{
		case TextureStorageType_None :
			{
				// NOTE(hugo) : Do nothing
			} break;
		case TextureStorageType_TextureData :
			{
				bitmap Texture = {};
				fread(&Texture.Width, sizeof(s32), 1, FileHandle);
				Assert(Texture.Width > 0);
				fread(&Texture.Height, sizeof(s32), 1, FileHandle);
				Assert(Texture.Height > 0);
				fread(&Texture.BytesPerPixels, sizeof(u32), 1, FileHandle);
				Assert(Texture.BytesPerPixels == 3);
				// TODO(hugo): Stop forcing 4 bps at some point
				Texture.BytesPerPixels = 4;
				u32 PixelCount = Texture.Width * Texture.Height;
				u32 TextureSizeInBytes = PixelCount * Texture.BytesPerPixels;
				Texture.Data = AllocateArray(u8, TextureSizeInBytes);
				for(u32 PixelIndex = 0; PixelIndex < PixelCount; ++PixelIndex)
				{
					u8* Pixel = (u8*)((u32*)Texture.Data + PixelIndex);
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					fread(Pixel, sizeof(u8), 1, FileHandle);
					++Pixel;
					*Pixel = 255;
				}

				Assert(TextureList->Count < ArrayCount(TextureList->Textures));
				TextureList->Textures[TextureList->Count] = Texture;
				// TODO(hugo): Store the normal map ID in the resulting mesh
				++TextureList->Count;
			} break;
		case TextureStorageType_TextureID :
			{
				fseek(FileHandle, sizeof(u32), SEEK_CUR);
			} break;
		InvalidDefaultCase;
	};
	return(Result);
}

inline void
PushMeshToList(mesh_list* MeshList, mesh Mesh)
{
	Assert(MeshList->Count < ArrayCount(MeshList->Meshes));
	MeshList->Meshes[MeshList->Count] = Mesh;

	CompletePreviousWritesBeforeFutureWrites;

	++MeshList->Count;
}

internal vk_mesh_buffer
VulkanCreateMeshBuffer(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkCommandPool CommandPool, VkQueue GraphicsQueue, mesh Mesh);

internal vk_attachment
VulkanCreateTextureAttachment(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkQueue GraphicsQueue, VkCommandPool CommandPool,
		bitmap TextureBitmap);

struct load_mesh_data
{
	mesh_list* MeshList;
	vk_mesh_buffer* MeshBuffers;
	texture_list* TextureList;
	char* Filename;
	VkDevice Device;
	VkPhysicalDevice PhysicalDevice;
	VkQueue GraphicsQueue;

	VkDescriptorBufferInfo* DescriptorBufferInfo;
	VkDescriptorSet* DescriptorSets;
	VkDescriptorImageInfo* DescriptorImageInfos;
	VkDescriptorImageInfo* ShadowMappingDescriptorImageInfo;
	u32 GraphicsQueueFamilyIndex;

	VkSampler* TextureSampler;
	vk_attachment* ImageTextures;
};

PLATFORM_WORK_QUEUE_CALLBACK(LoadMeshes)
{
	load_mesh_data* LoadMeshData = (load_mesh_data*)Data;

	// TODO(hugo): Not thrilled about having to reload the file
	// each time. Maybe read it all in a buffer beforehand.
	FILE* MSHFileHandle = fopen(LoadMeshData->Filename, "rb");
	Assert(MSHFileHandle);

	u32 MeshCount = 0;
	fread(&MeshCount, sizeof(u32), 1, MSHFileHandle);
	Assert(MeshCount > 0);
	Assert(MeshCount < ArrayCount(LoadMeshData->MeshList->Meshes));

	// NOTE(hugo): Command Pool Creation
	// {
	VkCommandPoolCreateInfo CommandPoolCreateInfo = {};
	CommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	CommandPoolCreateInfo.queueFamilyIndex = LoadMeshData->GraphicsQueueFamilyIndex;
	CommandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	VkCommandPool CommandPool = {};
	VK_CHECK(vkCreateCommandPool(LoadMeshData->Device, &CommandPoolCreateInfo, 0, &CommandPool));
	// }

	for(u32 MeshIndex = 0; MeshIndex < MeshCount; ++MeshIndex)
	{
		mesh Mesh = LoadMSHFromFile(MSHFileHandle, LoadMeshData->TextureList);

		// TODO(hugo): We need to push the mesh in the list but only
		// update the count at the very end (after a barrier) to be sure
		// we are safe
		Assert(LoadMeshData->MeshList->Count < ArrayCount(LoadMeshData->MeshList->Meshes));
		LoadMeshData->MeshList->Meshes[LoadMeshData->MeshList->Count] = Mesh;

		LoadMeshData->MeshBuffers[MeshIndex] = VulkanCreateMeshBuffer(LoadMeshData->Device,
				LoadMeshData->PhysicalDevice,
				CommandPool, LoadMeshData->GraphicsQueue,
				LoadMeshData->MeshList->Meshes[MeshIndex]);

		// NOTE(hugo): Load texture if not loaded yet
		// {
		u32 TextureIndex = LoadMeshData->MeshList->Meshes[MeshIndex].TextureID;
		if(LoadMeshData->ImageTextures[TextureIndex].ImageView == VK_NULL_HANDLE)
		{
			// NOTE(hugo): Load the texture
			LoadMeshData->ImageTextures[TextureIndex] =
				VulkanCreateTextureAttachment(LoadMeshData->Device,
						LoadMeshData->PhysicalDevice, LoadMeshData->GraphicsQueue,
					CommandPool,
					LoadMeshData->TextureList->Textures[TextureIndex]);
			Assert(LoadMeshData->ImageTextures[TextureIndex].ImageView != VK_NULL_HANDLE);

			// NOTE(hugo): Create the descripor
			LoadMeshData->DescriptorImageInfos[TextureIndex].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			LoadMeshData->DescriptorImageInfos[TextureIndex].imageView = LoadMeshData->ImageTextures[TextureIndex].ImageView;
			LoadMeshData->DescriptorImageInfos[TextureIndex].sampler = *LoadMeshData->TextureSampler;
		}
		// }

		// NOTE(hugo): I wonder if the patter should be :
		//  - Have just one descriptor set for the meta-data (ubo size, image view count, ...)
		//  - In the command buffer generation, just before using the descriptor set, write it
		//     with the actual value you need for the mesh
		VkWriteDescriptorSet WriteDescriptorSets[3] = {};
		WriteDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		WriteDescriptorSets[0].dstSet = LoadMeshData->DescriptorSets[MeshIndex];
		WriteDescriptorSets[0].dstBinding = 0;
		WriteDescriptorSets[0].dstArrayElement = 0;
		WriteDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		WriteDescriptorSets[0].descriptorCount = 1;
		WriteDescriptorSets[0].pBufferInfo = LoadMeshData->DescriptorBufferInfo;
		WriteDescriptorSets[0].pImageInfo = 0;
		WriteDescriptorSets[0].pTexelBufferView = 0;

		WriteDescriptorSets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		WriteDescriptorSets[1].dstSet = LoadMeshData->DescriptorSets[MeshIndex];
		WriteDescriptorSets[1].dstBinding = 1;
		WriteDescriptorSets[1].dstArrayElement = 0;
		WriteDescriptorSets[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		WriteDescriptorSets[1].descriptorCount = 1;
		WriteDescriptorSets[1].pBufferInfo = 0;

		// NOTE(hugo): We have a descriptor image info per texture
		// and Descriptor Set per mesh (is it even the best ?), so that 
		// is the line where we make the connection between the two
		// TODO(hugo): This is the only place where we use the MeshList for
		// something else than the Loaded Mesh Count
		WriteDescriptorSets[1].pImageInfo = &LoadMeshData->DescriptorImageInfos[LoadMeshData->MeshList->Meshes[MeshIndex].TextureID];
		WriteDescriptorSets[1].pTexelBufferView = 0;

		WriteDescriptorSets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		WriteDescriptorSets[2].dstSet = LoadMeshData->DescriptorSets[MeshIndex];
		WriteDescriptorSets[2].dstBinding = 2;
		WriteDescriptorSets[2].dstArrayElement = 0;
		WriteDescriptorSets[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		WriteDescriptorSets[2].descriptorCount = 1;
		WriteDescriptorSets[2].pBufferInfo = 0;
		WriteDescriptorSets[2].pImageInfo = LoadMeshData->ShadowMappingDescriptorImageInfo;
		WriteDescriptorSets[2].pTexelBufferView = 0;
		vkUpdateDescriptorSets(LoadMeshData->Device, ArrayCount(WriteDescriptorSets), WriteDescriptorSets, 0, 0);
		CompletePreviousWritesBeforeFutureWrites;

		++LoadMeshData->MeshList->Count;
	}

	vkDestroyCommandPool(LoadMeshData->Device, CommandPool, 0);

	fclose(MSHFileHandle);
}

internal u32
LoadMSHFile(load_mesh_data* LoadMeshData, platform_work_queue* Queue,
		VkDescriptorSetLayout UniformLayout, VkDescriptorPool* DescriptorPool)
{
	FILE* MSHFileHandle = fopen(LoadMeshData->Filename, "rb");
	Assert(MSHFileHandle);

	u32 MeshCount = 0;
	fread(&MeshCount, sizeof(u32), 1, MSHFileHandle);
	Assert(MeshCount > 0);
	Assert(MeshCount < ArrayCount(LoadMeshData->MeshList->Meshes));

	fclose(MSHFileHandle);

	VkDescriptorPoolSize DescriptorPoolSizes[2] = {};
	DescriptorPoolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	DescriptorPoolSizes[0].descriptorCount = MeshCount + 1;
	DescriptorPoolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	DescriptorPoolSizes[1].descriptorCount = 2 * MeshCount + 1;

	VkDescriptorPoolCreateInfo DescriptorPoolCreateInfo = {};
	DescriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	DescriptorPoolCreateInfo.poolSizeCount = ArrayCount(DescriptorPoolSizes);
	DescriptorPoolCreateInfo.pPoolSizes = DescriptorPoolSizes;
	// TODO(hugo): Can I do only one set but update it each time I
	// render a new mesh ? Which one is best ?
	DescriptorPoolCreateInfo.maxSets = MeshCount + 2; // TODO(hugo): not sure I understood this
	DescriptorPoolCreateInfo.flags = 0;

	VK_CHECK(vkCreateDescriptorPool(LoadMeshData->Device, &DescriptorPoolCreateInfo, 0, DescriptorPool));

	VkDescriptorSetLayout Layouts[] = {UniformLayout};
	VkDescriptorSetAllocateInfo DescriptorSetAllocateInfo = {};
	DescriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	DescriptorSetAllocateInfo.descriptorPool = *DescriptorPool;
	DescriptorSetAllocateInfo.descriptorSetCount = ArrayCount(Layouts);
	DescriptorSetAllocateInfo.pSetLayouts = Layouts;

	for(u32 DescriptorIndex = 0; DescriptorIndex < MeshCount; ++DescriptorIndex)
	{
		VK_CHECK(vkAllocateDescriptorSets(LoadMeshData->Device,
					&DescriptorSetAllocateInfo,
					&LoadMeshData->DescriptorSets[DescriptorIndex]));
	}

	SDLAddEntry(Queue, LoadMeshes, LoadMeshData);
	return(MeshCount);
}

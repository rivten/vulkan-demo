@echo off

rem --------------------------------------------------------------------------
rem                        COMPILATION
rem --------------------------------------------------------------------------

set SDLPath=C:\SDL2-2.0.7\
set VulkanPath=C:\VulkanSDK\1.1.77.0\
set VulkanLibPath=%VulkanPath%\Lib\
set VulkanIncludePath=%VulkanPath%\Include\vulkan\
set SDLBinPath=%SDLPath%\lib\x64\
rem set SDLNetPath=C:\SDL2_net-2.0.1\
rem set SDLNetBinPath=%SDLNetPath%\lib\x64\

set ShaderCompilerPath=%VulkanPath%\Bin\
set CompileShader=%ShaderCompilerPath%\glslangValidator.exe -V

set STBPath=..\..\stb\
set RivtenPath=..\..\rivten\

set UntreatedWarnings=/wd4100 /wd4244 /wd4201 /wd4127 /wd4505 /wd4456 /wd4996 /wd4003 /wd4706
set CommonCompilerDebugFlags=/MT /O2 /Oi /fp:fast /fp:except- /Zo /Gm- /GR- /EHa /WX /W4 %UntreatedWarnings% /Z7 /nologo /I %SDLPath%\include\ /I %STBPath% /I %RivtenPath% /I %VulkanIncludePath%
set CommonLinkerDebugFlags=/incremental:no /opt:ref /subsystem:console %SDLBinPath%\SDL2.lib %SDLBinPath%\SDL2main.lib %VulkanLibPath%\vulkan-1.lib /ignore:4099

pushd ..\build\
cl %CommonCompilerDebugFlags% ..\code\sdl_vulkan.cpp /link %CommonLinkerDebugFlags%
%CompileShader% ../code/triangle.vert -o triangle.vert.spv
%CompileShader% ../code/triangle.frag -o triangle.frag.spv
%CompileShader% ../code/toon.vert -o toon.vert.spv
%CompileShader% ../code/shadowmapping.vert -o shadowmapping.vert.spv
%CompileShader% ../code/shadowmapping.frag -o shadowmapping.frag.spv
%CompileShader% ../code/tonemapping.vert -o tonemapping.vert.spv
%CompileShader% ../code/tonemapping.frag -o tonemapping.frag.spv
popd

rem --------------------------------------------------------------------------
echo Compilation completed...

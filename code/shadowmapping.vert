#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal_In;
layout(location = 2) in vec2 UV_In;

layout(binding = 0) uniform shadow_mapping_ubo
{
	mat4 LightMVP;
} ShadowMappingUbo;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	gl_Position = ShadowMappingUbo.LightMVP * vec4(Position, 1.0f);
}

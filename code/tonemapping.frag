#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 UV;

layout(binding = 1) uniform sampler2D TextureSampler;

layout(location = 0) out vec4 FragmentColor;

// NOTE(hugo): Filmic tonemapping
// Code shamelessly taken from 
// http://duikerresearch.com/2015/09/filmic-tonemapping-for-real-time-rendering/

const float Gamma = 2.2f;
const float A = 0.15f;
const float B = 0.50f;
const float C = 0.10f;
const float D = 0.20f;
const float E = 0.02f;
const float F = 0.30f;
const float W = 11.2f;

vec3 Uncharted2Tonemap(vec3 x)
{
	return(((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F);
}

void main()
{
	vec3 TexColor = texture(TextureSampler, UV).xyz;
	float ExposureBias = 2.0f;
	vec3 Current = ExposureBias * Uncharted2Tonemap(TexColor);
	vec3 WhiteScale = 1.0f / Uncharted2Tonemap(vec3(W, W, W));
	vec3 Color = Current * WhiteScale;
	FragmentColor = vec4(pow(Color, vec3(1.0f / Gamma)), 1.0f);
}

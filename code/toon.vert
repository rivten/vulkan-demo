#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Color;
layout(location = 2) in vec2 UVIn;

layout(location = 0) out vec3 VertexColor;
layout(location = 1) out vec2 UV;

layout(binding = 0) uniform ubo
{
	mat4 MVP;
	float Time;
} Ubo;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	gl_Position = Ubo.MVP * vec4(Position, 1.0f);
	VertexColor = vec3(1.0f, 0.0f, 0.0f);
	UV = UVIn;
}

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 VertexPosWS;
layout(location = 1) in vec3 NormalWS;
layout(location = 2) in vec2 UV;
layout(location = 3) in vec4 FragPosLPS;

layout(binding = 1) uniform sampler2D TextureSampler;
layout(binding = 2) uniform sampler2D ShadowMapSampler;

layout(location = 0) out vec4 FragmentColor;

vec4
Grey(float Value)
{
	return(vec4(Value, Value, Value, 1.0f));
}

const float Gamma = 2.2f;
const float OneOverGamma = 1.0f / Gamma;
const float DepthBias = 0.01f;

// NOTE(hugo): Shadow value :
//      - 0 no shadow (in light), 1 full shadow
float
GetShadowValue(vec4 FragPosLPS)
{
	float ShadowValue = 0.0f;

	vec3 FragPosClipSpace = 0.5f * FragPosLPS.xyz / FragPosLPS.w + vec3(0.5f, 0.5f, 0.5f);
	float DepthFromLight = FragPosClipSpace.z;
	float ClosestDepthInDir = texture(ShadowMapSampler, FragPosClipSpace.xy).r;
	if(DepthFromLight > 1.0f)
	{
		ShadowValue = 0.0f;
	}
	else
	{
		vec2 TexelSize = 1.0f / textureSize(ShadowMapSampler, 0);
		for(int X = -1; X <= 1; ++X)
		{
			for(int Y = -1; Y <= 1; ++Y)
			{
				float ClosestDepthInDir = texture(ShadowMapSampler, FragPosClipSpace.xy + TexelSize * vec2(X, Y)).r;
				if(ClosestDepthInDir < DepthFromLight - DepthBias)
				{
					ShadowValue += 1.0f;
				}
			}
		}
	}
	ShadowValue /= 9.0f;
	return(ShadowValue);
}

void main()
{
	vec3 LightPosWS = vec3(0.0f, 30.0f, 0.0f);
	vec3 LightDirWS = normalize(LightPosWS - VertexPosWS);
	float LightDirDotNormal = clamp(dot(LightDirWS, NormalWS), 0.0f, 1.0f);
	float ShadowValue = GetShadowValue(FragPosLPS);
	vec4 Color = (1.0f - ShadowValue) * LightDirDotNormal *
			pow(texture(TextureSampler, 1.0f * UV), vec4(Gamma));
	//FragmentColor = pow(Color, vec4(OneOverGamma));
	FragmentColor = Color;
}

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal_In;
layout(location = 2) in vec2 UV_In;

layout(location = 0) out vec3 VertexPosWS;
layout(location = 1) out vec3 Normal;
layout(location = 2) out vec2 UV;
layout(location = 3) out vec4 FragPosLPS;

layout(binding = 0) uniform ubo
{
	mat4 MVP;
	mat4 LightMVP;
} Ubo;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	VertexPosWS = Position;
	gl_Position = Ubo.MVP * vec4(Position, 1.0f);
	Normal = Normal_In;
	UV = UV_In;
	FragPosLPS = Ubo.LightMVP * vec4(Position, 1.0f);
}

#ifdef _WIN32
#include <SDL.h>
#include <SDL_syswm.h>
#define VK_USE_PLATFORM_WIN32_KHR
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#define VK_USE_PLATFORM_XCB_KHR
#include <X11/Xlib-xcb.h>
#endif

#include <rivten.h>
#include <rivten_math.h>
#include "vulkan.h"

#include "multithreading.h"

#define VALIDATION_LAYER 0
#define MAX_U64 0xFFFFFFFFFFFFFFFF
#define VULKAN_DEPTH_FORMAT VK_FORMAT_D32_SFLOAT_S8_UINT // TODO(hugo): Should be retrieved from the actual VkImageCreateInfo ?
#define SHADOWMAP_DIM 2048

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

/*
 TODO(hugo):
	* v0 Release
		- ImGui integration
		- Material system (BRDF at least ?)
		- Post FX proof of concept
		- Multi threaded command buffer generation
		- Light system
		- Gamma correction
		- Tokiwa back-integration :)
*/

global_variable u32 GlobalWindowWidth = 960;
global_variable u32 GlobalWindowHeight = 580;
global_variable b32 GlobalRunning = true;

global_variable const float ScreenRectVertices[] = 
{ 
	// Positions   // TexCoords
	-1.0f,  1.0f,  0.0f, 1.0f,
	-1.0f, -1.0f,  0.0f, 0.0f,
	 1.0f, -1.0f,  1.0f, 0.0f,

	-1.0f,  1.0f,  0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f, 0.0f,
	 1.0f,  1.0f,  1.0f, 1.0f
};	

#define VK_CHECK(Call) {VkResult OpResult = Call; Assert(OpResult == VK_SUCCESS);}

u32 GetFileSizeInBytes(FILE* f)
{
	// NOTE(hugo) : This resets the cursor at the beginning
	Assert(f);
	fseek(f, 0, SEEK_SET);
	fseek(f, 0, SEEK_END);

	// TODO(hugo) : Achtung ! Internet says this is not cross-platform
	// and should only be used with binary files. Why does it
	// work for me here ?
	u32 FileSize = ftell(f);

	fseek(f, 0, SEEK_SET);

	return(FileSize);
}

struct bitmap
{
	s32 Width;
	s32 Height;
	u32 BytesPerPixels;
	u8* Data;
};

struct vk_buffer
{
	VkBuffer Buffer;
	VkDeviceMemory Memory;
};

struct vk_mesh_buffer
{
	vk_buffer Vertex;
	vk_buffer Index;
	u32 IndexCount;
};

struct vk_attachment
{
	VkImage Image;
	VkImageView ImageView;
	VkDeviceMemory Memory;
};

internal VkFramebuffer
VulkanCreateFramebuffer(VkDevice Device, VkImageView* Views, u32 ViewCount,
		VkRenderPass RenderPass, u32 Width, u32 Height)
{
	VkFramebuffer Result = {};

	VkFramebufferCreateInfo FramebufferCreateInfo = {};
	FramebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	FramebufferCreateInfo.renderPass = RenderPass;
	FramebufferCreateInfo.attachmentCount = ViewCount;
	FramebufferCreateInfo.pAttachments = Views;
	FramebufferCreateInfo.width = Width;
	FramebufferCreateInfo.height = Height;
	FramebufferCreateInfo.layers = 1;

	VK_CHECK(vkCreateFramebuffer(Device, &FramebufferCreateInfo, 0, &Result));

	return(Result);
}

#include "mesh.cpp"

#define VULKAN_INVALID_MEMORY_TYPE_INDEX 0xFFFFFFFF
internal u32
VulkanFindMemoryTypeIndex(VkPhysicalDevice PhysicalDevice,
		VkMemoryPropertyFlags MemoryPropertyFlags,
		VkMemoryRequirements MemoryRequirements)
{
	VkPhysicalDeviceMemoryProperties PhysicalDeviceMemProperties = {};
	vkGetPhysicalDeviceMemoryProperties(PhysicalDevice, &PhysicalDeviceMemProperties);

	u32 SelectedMemoryTypeIndex = VULKAN_INVALID_MEMORY_TYPE_INDEX;
	for(u32 MemoryTypeIndex = 0;
			MemoryTypeIndex < PhysicalDeviceMemProperties.memoryTypeCount;
			++MemoryTypeIndex)
	{
		VkMemoryPropertyFlags CurrentMemoryProperties =
			PhysicalDeviceMemProperties.memoryTypes[MemoryTypeIndex].propertyFlags;

		if((MemoryRequirements.memoryTypeBits & (1 << MemoryTypeIndex)) &&
				((CurrentMemoryProperties & MemoryPropertyFlags) == MemoryPropertyFlags))
		{
			SelectedMemoryTypeIndex = MemoryTypeIndex;
			break;
		}
	}
	Assert(SelectedMemoryTypeIndex != VULKAN_INVALID_MEMORY_TYPE_INDEX);

	return(SelectedMemoryTypeIndex);
}

vk_buffer
VulkanCreateBuffer(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkDeviceSize DeviceSize, 
		VkBufferUsageFlags BufferUsageFlags,
		VkMemoryPropertyFlags MemoryPropertyFlags)
{
	Assert(DeviceSize > 0);

	vk_buffer Result = {};

	VkBufferCreateInfo BufferCreateInfo = {};
	BufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	BufferCreateInfo.size = DeviceSize;
	BufferCreateInfo.usage = BufferUsageFlags;
	BufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	BufferCreateInfo.flags = 0;

	VK_CHECK(vkCreateBuffer(Device, &BufferCreateInfo, 0, &Result.Buffer));

	VkMemoryRequirements BufferMemoryRequirements = {};
	vkGetBufferMemoryRequirements(Device, Result.Buffer, &BufferMemoryRequirements);
	
	// TODO(hugo): Maybe we could store the physical device memory properties
	// to avoid passing the physical device and query the properties each time
	// we create a buffer ?

	VkMemoryAllocateInfo MemAllocInfo = {};
	MemAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	MemAllocInfo.allocationSize = BufferMemoryRequirements.size;
	MemAllocInfo.memoryTypeIndex = VulkanFindMemoryTypeIndex(PhysicalDevice, 
			MemoryPropertyFlags, BufferMemoryRequirements);

	// TODO(hugo): You should not use vkAllocate memory for every
	// buffer creation. Instead we should batch those call to
	// allocate one big chunk of memory (I think ?)
	// See: https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator
	VK_CHECK(vkAllocateMemory(Device, &MemAllocInfo, 0, &Result.Memory));
	VK_CHECK(vkBindBufferMemory(Device, Result.Buffer, Result.Memory, 0));

	return(Result);
}

void
VulkanDestroyBuffer(VkDevice Device, vk_buffer Buffer)
{
	vkFreeMemory(Device, Buffer.Memory, 0);
	vkDestroyBuffer(Device, Buffer.Buffer, 0);
}

void
VulkanSendMemoryToBuffer(VkDevice Device, VkDeviceMemory Memory, u32 Size, void* Data)
{
	void* MappedMemory = 0;
	VK_CHECK(vkMapMemory(Device, Memory, 0, Size, 0, &MappedMemory));
	memcpy(MappedMemory, Data, Size);
	vkUnmapMemory(Device, Memory);
}

mat4
VulkanPerspective(float FoV, float Aspect, float NearPlane, float FarPlane)
{
	mat4 Result = {};

	Assert(FoV > 0 && Aspect != 0);

	float FrustrumDepth = FarPlane - NearPlane;
	float OneOverDepth = 1.0 / FrustrumDepth;
	float OneOverTanHalfFoV = 1.0f / Tan(0.5f * FoV);

	Result.Data_[0] = OneOverTanHalfFoV / Aspect;
	Result.Data_[1] = 0.0f;
	Result.Data_[2] = 0.0f;
	Result.Data_[3] = 0.0f;

	Result.Data_[4] = 0.0f;
	Result.Data_[5] = - OneOverTanHalfFoV;
	Result.Data_[6] = 0.0f;
	Result.Data_[7] = 0.0f;

	Result.Data_[8] = 0.0f;
	Result.Data_[9] = 0.0f;
	Result.Data_[10] = - FarPlane * OneOverDepth;
	Result.Data_[11] = -1.0f;

	Result.Data_[12] = 0.0f;
	Result.Data_[13] = 0.0f;
	Result.Data_[14] = - FarPlane * NearPlane * OneOverDepth;
	Result.Data_[15] = 0.0f;

	return(Result);
}

struct ubo
{
	mat4 MVP;
	mat4 LightMVP;
	float Time;
};

struct shadow_mapping_ubo
{
	mat4 LightMVP;
};

internal VkShaderModule
VulkanCreateShaderModule(char* ShaderFilename, VkDevice Device)
{
	FILE* ShaderFileHandle = fopen(ShaderFilename, "rb");
	Assert(ShaderFileHandle);
	u32 CodeSize = GetFileSizeInBytes(ShaderFileHandle);
	char* ShaderCode = AllocateArray(char, CodeSize);
	fread(ShaderCode, CodeSize, 1, ShaderFileHandle);
	VkShaderModuleCreateInfo ShaderCreateInfo = {};
	ShaderCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	ShaderCreateInfo.codeSize = CodeSize;
	ShaderCreateInfo.pCode = (const u32 *)ShaderCode;

	VkShaderModule ShaderModule = {};
	VK_CHECK(vkCreateShaderModule(Device, &ShaderCreateInfo, 0, &ShaderModule));

	Free(ShaderCode);
	fclose(ShaderFileHandle);

	return(ShaderModule);
}

struct vk_shader_stage
{
	VkShaderModule VertexModule;
	VkShaderModule FragmentModule;
	VkPipelineShaderStageCreateInfo VertexCreateInfo;
	VkPipelineShaderStageCreateInfo FragmentCreateInfo;
};

// TODO(hugo): What if I want to create a shader stage
// with a new vertex shader but an already create fragment
// shader module ? (or the other way around)
internal vk_shader_stage
VulkanCreateShaderStage(VkDevice Device, char* VertexFilename, char* FragmentFilename)
{
	VkShaderModule VertexShaderModule = VulkanCreateShaderModule(VertexFilename, Device);
	VkShaderModule FragmentShaderModule = VulkanCreateShaderModule(FragmentFilename, Device);

	VkPipelineShaderStageCreateInfo VertexShaderStageCreateInfo = {};
	VertexShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	VertexShaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	VertexShaderStageCreateInfo.module = VertexShaderModule;
	VertexShaderStageCreateInfo.pName = "main";

	VkPipelineShaderStageCreateInfo FragmentShaderStageCreateInfo = {};
	FragmentShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	FragmentShaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	FragmentShaderStageCreateInfo.module = FragmentShaderModule;
	FragmentShaderStageCreateInfo.pName = "main";

	vk_shader_stage Result = {};
	Result.VertexModule = VertexShaderModule;
	Result.FragmentModule = FragmentShaderModule;
	Result.VertexCreateInfo = VertexShaderStageCreateInfo;
	Result.FragmentCreateInfo = FragmentShaderStageCreateInfo;

	return(Result);
}

VkCommandBuffer
VulkanBeginSingleTimeCommands(VkDevice Device, VkCommandPool CommandPool)
{
	VkCommandBufferAllocateInfo CommandBufferAllocInfo = {};
	CommandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	CommandBufferAllocInfo.commandPool = CommandPool;
	CommandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	CommandBufferAllocInfo.commandBufferCount = 1;

	VkCommandBuffer CommandBuffer = {};
	VK_CHECK(vkAllocateCommandBuffers(Device, &CommandBufferAllocInfo, &CommandBuffer));

	VkCommandBufferBeginInfo CommandBufferBeginInfo = {};
	CommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	CommandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	CommandBufferBeginInfo.pInheritanceInfo = 0;

	vkBeginCommandBuffer(CommandBuffer, &CommandBufferBeginInfo);

	return(CommandBuffer);
}

void
VulkanEndSingleTimeCommands(VkDevice Device, VkQueue Queue, VkCommandPool CommandPool, VkCommandBuffer CommandBuffer)
{
	vkEndCommandBuffer(CommandBuffer);

	VkFenceCreateInfo FenceCreateInfo = {};
	FenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	FenceCreateInfo.flags = 0;
	VkFence Fence = {};
	VK_CHECK(vkCreateFence(Device, &FenceCreateInfo, 0, &Fence));

	VkSubmitInfo CopySubmitInfo = {};
	CopySubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	CopySubmitInfo.commandBufferCount = 1;
	CopySubmitInfo.pCommandBuffers = &CommandBuffer;

	VK_CHECK(vkQueueSubmit(Queue, 1, &CopySubmitInfo, Fence));
	VK_CHECK(vkWaitForFences(Device, 1, &Fence, VK_TRUE, MAX_U64));
	vkQueueWaitIdle(Queue);
	vkFreeCommandBuffers(Device, CommandPool, 1, &CommandBuffer);
	vkDestroyFence(Device, Fence, 0);
}

static VKAPI_ATTR VkBool32 VKAPI_CALL 
ValidationDebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
	fprintf(stdout, "--- Validation Layer ---\n %s\n", msg);
	Assert(false);
	return(VK_FALSE);
}

struct vk_pipeline_example
{
	VkPipeline Phong;
	VkPipeline Wireframe;
	VkPipeline Toon;
};

bitmap LoadBitmap(const char* Filename, u32 OutputChannel = 3)
{
	bitmap Result = {};
	s32 BytesPerPixels;
	Result.Data = stbi_load(Filename, &Result.Width, &Result.Height, &BytesPerPixels, OutputChannel);
	Result.BytesPerPixels = OutputChannel;
	Assert(Result.Data);

	return(Result);
}

void FreeBitmap(bitmap Bitmap)
{
	stbi_image_free(Bitmap.Data);
}

internal VkImageView
VulkanCreateSimpleImageView(VkDevice Device,
		VkImage Image, VkFormat Format,
		VkImageAspectFlags ImageAspectFlags)
{
	VkImageViewCreateInfo ImageViewCreateInfo = {};
	ImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	ImageViewCreateInfo.image = Image;
	ImageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	ImageViewCreateInfo.format = Format;
	ImageViewCreateInfo.subresourceRange.aspectMask = ImageAspectFlags;
	ImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
	ImageViewCreateInfo.subresourceRange.levelCount = 1;
	ImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
	ImageViewCreateInfo.subresourceRange.layerCount = 1;
	ImageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	ImageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	ImageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	ImageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

	VkImageView ImageView = {};
	VK_CHECK(vkCreateImageView(Device, &ImageViewCreateInfo, 0, &ImageView));

	return(ImageView);
}

internal void
VulkanTransitionImageLayout(VkDevice Device, VkQueue GraphicsQueue,
		VkCommandPool CommandPool, VkImage Image,
		VkImageLayout OldLayout, VkImageLayout NewLayout,
		VkImageAspectFlags ImageAspectFlags,
		VkPipelineStageFlags SourceStage, VkPipelineStageFlags DestStage,
		VkAccessFlags SourceAccessMask, VkAccessFlags DestAccessMask)
{
	VkCommandBuffer ImageCommandBuffer = VulkanBeginSingleTimeCommands(Device, CommandPool);

	VkImageMemoryBarrier ImageMemoryBarrier = {};
	ImageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	ImageMemoryBarrier.oldLayout = OldLayout;
	ImageMemoryBarrier.newLayout = NewLayout;
	ImageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	ImageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	ImageMemoryBarrier.image = Image;
	ImageMemoryBarrier.subresourceRange.aspectMask = ImageAspectFlags;
	// TODO(hugo): This should probably be linked with the actual image data ?
	ImageMemoryBarrier.subresourceRange.baseMipLevel = 0;
	ImageMemoryBarrier.subresourceRange.levelCount = 1;
	ImageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
	ImageMemoryBarrier.subresourceRange.layerCount = 1;
	ImageMemoryBarrier.srcAccessMask = SourceAccessMask;
	ImageMemoryBarrier.dstAccessMask = DestAccessMask;

	vkCmdPipelineBarrier(ImageCommandBuffer, SourceStage, DestStage, 0, 0, 0, 0, 0, 1, &ImageMemoryBarrier);

	VulkanEndSingleTimeCommands(Device, GraphicsQueue, CommandPool, ImageCommandBuffer);
}

enum vk_image_transition_layout_type
{
	VulkanImageTransitionLayoutType_UndefToDstOptimal,
	VulkanImageTransitionLayoutType_DstOptimalToShaderReadOnlyOptimal,
	VulkanImageTransitionLayoutType_UndefToDepthStencilOptimal,

	VulkanImageTransitionLayoutType_Count,
};

internal void
VulkanTransitionImageLayout(VkDevice Device, VkQueue GraphicsQueue,
		VkCommandPool CommandPool, VkImage Image,
		VkImageAspectFlags ImageAspectFlags,
		vk_image_transition_layout_type ImageTransitionLayoutType)
{
	VkImageLayout OldLayout = {};
	VkImageLayout NewLayout = {};
	VkPipelineStageFlags SourceStage = {};
	VkPipelineStageFlags DestStage = {};
	VkAccessFlags SourceAccessMask = {};
	VkAccessFlags DestAccessMask = {};

	switch(ImageTransitionLayoutType)
	{
		case VulkanImageTransitionLayoutType_UndefToDstOptimal:
			{
				OldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				NewLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
				SourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				DestStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
				SourceAccessMask = 0;
				DestAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			} break;
		case VulkanImageTransitionLayoutType_DstOptimalToShaderReadOnlyOptimal:
			{
				OldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
				NewLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				SourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
				DestStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
				SourceAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				DestAccessMask = VK_ACCESS_SHADER_READ_BIT;
			} break;
		case VulkanImageTransitionLayoutType_UndefToDepthStencilOptimal:
			{
				OldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				NewLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
				SourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				DestStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
				SourceAccessMask = 0;
				DestAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
					VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			} break;
		InvalidDefaultCase;
	}

	VulkanTransitionImageLayout(Device, GraphicsQueue,
			CommandPool, Image, OldLayout, NewLayout,
			ImageAspectFlags, SourceStage, DestStage,
			SourceAccessMask, DestAccessMask);
}

internal VkCommandBuffer
VulkanCreatePrimaryCommandBuffer(VkDevice Device, VkCommandPool CommandPool)
{
	VkCommandBuffer Result = {};

	VkCommandBufferAllocateInfo PrimaryCommandBufferAllocateInfo = {};
	PrimaryCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	PrimaryCommandBufferAllocateInfo.commandPool = CommandPool;
	PrimaryCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	PrimaryCommandBufferAllocateInfo.commandBufferCount = 1;

	VK_CHECK(vkAllocateCommandBuffers(Device, &PrimaryCommandBufferAllocateInfo, &Result));

	return(Result);
}

struct vk_command_buffer_hierarchy
{
	u32 SecondaryCount;
	VkCommandBuffer Primary;
	VkCommandBuffer* Secondary;
};

internal vk_command_buffer_hierarchy
VulkanCreateCommandBufferHierarchy(VkDevice Device, VkCommandPool CommandPool, u32 Count)
{
	vk_command_buffer_hierarchy Result = {};

	Result.SecondaryCount = Count;

	Result.Primary = VulkanCreatePrimaryCommandBuffer(Device, CommandPool);

	VkCommandBufferAllocateInfo SecondaryCommandBufferAllocateInfo = {};
	SecondaryCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	SecondaryCommandBufferAllocateInfo.commandPool = CommandPool;
	SecondaryCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
	SecondaryCommandBufferAllocateInfo.commandBufferCount = Count;

	Result.Secondary = AllocateArray(VkCommandBuffer, Count);
	VK_CHECK(vkAllocateCommandBuffers(Device, &SecondaryCommandBufferAllocateInfo, Result.Secondary));

	return(Result);
}

enum game_input_mouse_button
{
	PlatformMouseButton_Left,
	PlatformMouseButton_Middle,
	PlatformMouseButton_Right,
	PlatformMouseButton_Extended0,
	PlatformMouseButton_Extended1,

	PlatformMouseButton_Count,
};

struct game_button_state
{
	int HalfTransitionCount;
	b32 EndedDown;
};

struct game_controller_input
{
    union
    {
        game_button_state Buttons[12];
        struct
        {
            game_button_state MoveUp;
            game_button_state MoveDown;
            game_button_state MoveLeft;
            game_button_state MoveRight;
            
            game_button_state ActionUp;
            game_button_state ActionDown;
            game_button_state ActionLeft;
            game_button_state ActionRight;
            
            game_button_state LeftShoulder;
            game_button_state RightShoulder;

            game_button_state Back;
            game_button_state Start;

            // NOTE(casey): All buttons must be added above this line
            
            game_button_state Terminator;
        };
    };
};

typedef float f32;
typedef float r32;

struct game_input
{
	f32 dtForFrame;
	b32 QuitRequested;

	// TODO(hugo) : Might change in the future to take
	// all input into account
	game_controller_input KeyboardController;

    game_button_state MouseButtons[PlatformMouseButton_Count];
	f32 MouseX;
	f32 MouseY;
	f32 MouseZ;
};

internal void
SDLProcessKeyboardEvent(game_button_state *NewState, b32 IsDown)
{
    if(NewState->EndedDown != IsDown)
    {
        NewState->EndedDown = IsDown;
        ++NewState->HalfTransitionCount;
    }
}

internal void
SDLProcessPendingEvents(game_controller_input *KeyboardController, game_input *Input)
{
    SDL_Event Event;
    for(;;)
    {
        int PendingEvents = 0;

        {
            PendingEvents = SDL_PollEvent(&Event);
        }

        if(!PendingEvents)
        {
            break;
        }

        switch(Event.type)
        {
            case SDL_QUIT:
            {
                GlobalRunning = false;
            } break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
            {
                SDL_Keycode KeyCode = Event.key.keysym.sym;

                //bool AltKeyWasDown = ((Event.key.keysym.mod & KMOD_ALT) != 0);
                //bool ShiftKeyWasDown = ((Event.key.keysym.mod & KMOD_SHIFT) != 0);
                bool IsDown = (Event.key.state == SDL_PRESSED);

                // NOTE: In the windows version, we used "if(IsDown != WasDown)"
                // to detect key repeats. SDL has the 'repeat' value, though,
                // which we'll use.
                if(Event.key.repeat == 0)
                {
                    if(KeyCode == SDLK_i)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->MoveUp, IsDown);
                    }
                    else if(KeyCode == SDLK_j)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->MoveLeft, IsDown);
                    }
                    else if(KeyCode == SDLK_k)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->MoveDown, IsDown);
                    }
                    else if(KeyCode == SDLK_l)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->MoveRight, IsDown);
                    }
                    else if(KeyCode == SDLK_u)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->LeftShoulder, IsDown);
                    }
                    else if(KeyCode == SDLK_o)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->RightShoulder, IsDown);
                    }
                    else if(KeyCode == SDLK_z)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->ActionUp, IsDown);
                    }
                    else if(KeyCode == SDLK_q)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->ActionLeft, IsDown);
                    }
                    else if(KeyCode == SDLK_s)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->ActionDown, IsDown);
                    }
                    else if(KeyCode == SDLK_d)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->ActionRight, IsDown);
                    }
                    else if(KeyCode == SDLK_ESCAPE)
                    {
                        SDLProcessKeyboardEvent(&KeyboardController->Back, IsDown);
                    }
                    else if(KeyCode == SDLK_SPACE)
                    {
                        //SDLProcessKeyboardEvent(&KeyboardController->Start, IsDown);
                        SDLProcessKeyboardEvent(&KeyboardController->RightShoulder, IsDown);
                    }
					else if(KeyCode == SDLK_RSHIFT)
					{
                        SDLProcessKeyboardEvent(&KeyboardController->LeftShoulder, IsDown);
					}
                }

            } break;
        }
    }
}

struct game_memory
{
	u64 PersistentStorageSize;
	void* PersistentStorage;

	u64 TransientStorageSize;
	void* TransientStorage;
};

struct camera
{
	v3 P;
	v3 XAxis;
	v3 ZAxis;
};

struct game_state
{
	float Time;
	camera Camera;
	v3 dPCamera;
	f32 CameraYawSpeed;
	f32 CameraPitchSpeed;

	b32 MouseDragging;
	v2 MouseCacheP;

	b32 IsInitialized;
};

internal vk_buffer
VulkanCreateAndSendBuffer(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkCommandPool CommandPool, VkQueue GraphicsQueue, void* Data, u32 DataSize,
		VkBufferUsageFlags BufferUsageFlags)
{
	vk_buffer Result = {};
	VkMemoryPropertyFlags RequiredProperties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	vk_buffer StagingBuffer = VulkanCreateBuffer(Device, PhysicalDevice,
			DataSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			RequiredProperties);

	VulkanSendMemoryToBuffer(Device, StagingBuffer.Memory, DataSize, Data);

	Result = VulkanCreateBuffer(Device, PhysicalDevice,
			DataSize,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | BufferUsageFlags,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	// NOTE(hugo): Copy from staging buffer to vertex buffer
	{
		VkCommandBuffer CopyCommandBuffer = VulkanBeginSingleTimeCommands(Device, CommandPool);

		VkBufferCopy BufferCopy = {};
		BufferCopy.srcOffset = 0;
		BufferCopy.dstOffset = 0;
		BufferCopy.size = DataSize;
		vkCmdCopyBuffer(CopyCommandBuffer, StagingBuffer.Buffer, Result.Buffer, 1, &BufferCopy);

		VulkanEndSingleTimeCommands(Device, GraphicsQueue, CommandPool, CopyCommandBuffer);
		VulkanDestroyBuffer(Device, StagingBuffer);
	}

	return(Result);
}

internal vk_mesh_buffer
VulkanCreateMeshBuffer(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkCommandPool CommandPool, VkQueue GraphicsQueue, mesh Mesh)
{
	vk_mesh_buffer Result = {};

	// NOTE(hugo): Vertex buffer creation & allocation
	// {
	Result.Vertex = VulkanCreateAndSendBuffer(Device, PhysicalDevice, CommandPool, GraphicsQueue,
			Mesh.Vertices, sizeof(vertex) * Mesh.VertexCount, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
	// }

	// NOTE(hugo): Index buffer
	// {

	Result.IndexCount = 3 * Mesh.TriangleCount;
	VkDeviceSize IndexBufferSize = sizeof(u32) * Result.IndexCount;

	Result.Index = VulkanCreateAndSendBuffer(Device, PhysicalDevice, CommandPool, GraphicsQueue,
			Mesh.Triangles, IndexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
	// }

	return(Result);
};

internal vk_attachment
VulkanCreateTextureAttachment(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		VkQueue GraphicsQueue, VkCommandPool CommandPool,
		bitmap TextureBitmap)
{
	vk_attachment Result = {};

	// NOTE(hugo): Texture Creation
	// {
	VkDeviceSize ImageSize = TextureBitmap.Width * TextureBitmap.Height * TextureBitmap.BytesPerPixels;
	
	vk_buffer ImageStagingBuffer = VulkanCreateBuffer(Device, PhysicalDevice, ImageSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	VulkanSendMemoryToBuffer(Device, ImageStagingBuffer.Memory, ImageSize, TextureBitmap.Data);
	FreeBitmap(TextureBitmap);

	VkImageCreateInfo ImageCreateInfo = {};
	ImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	ImageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	ImageCreateInfo.extent.width = TextureBitmap.Width;
	ImageCreateInfo.extent.height = TextureBitmap.Height;
	ImageCreateInfo.extent.depth = 1;
	ImageCreateInfo.mipLevels = 1;
	ImageCreateInfo.arrayLayers = 1;
	ImageCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	ImageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	ImageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	ImageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	ImageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	ImageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	ImageCreateInfo.flags = 0;

	VkImage TextureImage = {};
	VK_CHECK(vkCreateImage(Device, &ImageCreateInfo, 0, &TextureImage));

	VkMemoryRequirements MemoryRequirements = {};
	vkGetImageMemoryRequirements(Device, TextureImage, &MemoryRequirements);
	VkMemoryPropertyFlags MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	VkMemoryAllocateInfo MemoryAllocateInfo = {};
	MemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	MemoryAllocateInfo.allocationSize = MemoryRequirements.size;
	MemoryAllocateInfo.memoryTypeIndex = VulkanFindMemoryTypeIndex(PhysicalDevice,
			MemoryPropertyFlags, MemoryRequirements);

	VkDeviceMemory TextureImageMemory = {};
	VK_CHECK(vkAllocateMemory(Device, &MemoryAllocateInfo, 0, &TextureImageMemory));
	VK_CHECK(vkBindImageMemory(Device, TextureImage, TextureImageMemory, 0));
	// }
	
	VulkanTransitionImageLayout(Device, GraphicsQueue, CommandPool, TextureImage,
			VK_IMAGE_ASPECT_COLOR_BIT, VulkanImageTransitionLayoutType_UndefToDstOptimal);

	// NOTE(hugo): Copy buffer to image
	// {
	VkCommandBuffer CommandBuffer = VulkanBeginSingleTimeCommands(Device, CommandPool);

	VkBufferImageCopy BufferImageCopy = {};
	BufferImageCopy.bufferOffset = 0;
	BufferImageCopy.bufferRowLength = 0;
	BufferImageCopy.bufferImageHeight = 0;
	BufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	BufferImageCopy.imageSubresource.mipLevel = 0;
	BufferImageCopy.imageSubresource.baseArrayLayer = 0;
	BufferImageCopy.imageSubresource.layerCount = 1;
	BufferImageCopy.imageOffset = {0, 0, 0};
	BufferImageCopy.imageExtent = {(u32)TextureBitmap.Width, (u32)TextureBitmap.Height, 1};

	vkCmdCopyBufferToImage(CommandBuffer, ImageStagingBuffer.Buffer, TextureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &BufferImageCopy);

	VulkanEndSingleTimeCommands(Device, GraphicsQueue, CommandPool, CommandBuffer);
	// }

	VulkanTransitionImageLayout(Device, GraphicsQueue, CommandPool, TextureImage,
			VK_IMAGE_ASPECT_COLOR_BIT, VulkanImageTransitionLayoutType_DstOptimalToShaderReadOnlyOptimal);
	VulkanDestroyBuffer(Device, ImageStagingBuffer);

	// NOTE(hugo): Create Texture Image View
	// {
	VkImageView TextureImageView = VulkanCreateSimpleImageView(Device,
			TextureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
	// }

	Result.Image = TextureImage;
	Result.ImageView = TextureImageView;
	Result.Memory = TextureImageMemory;

	return(Result);
}

internal VkDeviceMemory
VulkanAllocateImageMemory(VkDevice Device, VkPhysicalDevice PhysicalDevice, VkImage Image)
{
	VkDeviceMemory Memory = {};

	VkMemoryRequirements MemoryRequirements = {};
	vkGetImageMemoryRequirements(Device, Image, &MemoryRequirements);
	VkMemoryPropertyFlags MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	VkMemoryAllocateInfo MemoryAllocateInfo = {};
	MemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	MemoryAllocateInfo.allocationSize = MemoryRequirements.size;
	MemoryAllocateInfo.memoryTypeIndex = VulkanFindMemoryTypeIndex(PhysicalDevice,
			MemoryPropertyFlags, MemoryRequirements);

	VK_CHECK(vkAllocateMemory(Device, &MemoryAllocateInfo, 0, &Memory));

	return(Memory);
}

internal vk_attachment
VulkanCreateAttachment2D(VkDevice Device, VkPhysicalDevice PhysicalDevice,
		u32 Width, u32 Height, VkFormat Format,
		VkImageUsageFlags Usage)
{
	vk_attachment Result = {};

	VkImageCreateInfo ImageCreateInfo = {};
	ImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	ImageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	ImageCreateInfo.extent.width = Width;
	ImageCreateInfo.extent.height = Height;
	ImageCreateInfo.extent.depth = 1;
	ImageCreateInfo.mipLevels = 1;
	ImageCreateInfo.arrayLayers = 1;
	ImageCreateInfo.format = Format;
	ImageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	// TODO(hugo): What to give for the initial layout ?
	//DepthImageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	ImageCreateInfo.usage = Usage | VK_IMAGE_USAGE_SAMPLED_BIT;
	ImageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	ImageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	ImageCreateInfo.flags = 0;

	VK_CHECK(vkCreateImage(Device, &ImageCreateInfo, 0, &Result.Image));

	Result.Memory = VulkanAllocateImageMemory(Device, PhysicalDevice, Result.Image);
	VK_CHECK(vkBindImageMemory(Device, Result.Image, Result.Memory, 0));

	VkImageAspectFlags AspectMask = 0;
	if(Usage & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
	{
		AspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}
	if(Usage & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
	{
		// TODO(hugo): Properly clean the stencil stuff
		//AspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
		AspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	}
	Assert(AspectMask != 0);

	Result.ImageView = VulkanCreateSimpleImageView(Device, 
			Result.Image, Format, AspectMask);

	return(Result);
}

internal void
VulkanDestroyAttachment2D(VkDevice Device, vk_attachment Attachment)
{
	vkDestroyImageView(Device, Attachment.ImageView, 0);
	vkDestroyImage(Device, Attachment.Image, 0);
	vkFreeMemory(Device, Attachment.Memory, 0);
}

internal void
GameUpdateAndRender(game_memory* Memory, game_input* Input,
		VkDevice Device, vk_buffer UniformBuffer, vk_buffer ShadowMappingUniformBuffer)
{
	Assert(sizeof(game_state) <= Memory->PersistentStorageSize);
	game_state* GameState = (game_state*)Memory->PersistentStorage;
	if(!GameState->IsInitialized)
	{
		GameState->Time = 0.0f;
		GameState->Camera.P = V3(0.0f, 0.0f, 10.0f);
		GameState->Camera.XAxis = V3(1.0f, 0.0f, 0.0f);
		GameState->Camera.ZAxis = V3(0.0f, 0.0f, 1.0f);
		GameState->dPCamera = V3(0.0f, 0.0f, 0.0f);
		GameState->CameraYawSpeed = 0.0f;
		GameState->CameraPitchSpeed = 0.0f;
		GameState->MouseDragging = false;
		GameState->MouseCacheP = V2(0.0f, 0.0f);

		GameState->IsInitialized = true;
	}

	GameState->Time += Input->dtForFrame;

	{
		// NOTE(hugo) : First-Person Camera movement
		f32 CameraDragScale = 10.0f;
		f32 CameraAcceleration = 50.0f;
		v3 ddP = V3(0.0f, 0.0f, 0.0f);
		f32 dt = Input->dtForFrame;
		v3 WorldY = V3(0.0f, 1.0f, 0.0f);
		if(Input->KeyboardController.MoveUp.EndedDown)
		{
			ddP += Normalized(-1.0f * GameState->Camera.ZAxis);
		}
		if(Input->KeyboardController.MoveDown.EndedDown)
		{
			ddP -= Normalized(-1.0f * GameState->Camera.ZAxis);
		}
		if(Input->KeyboardController.MoveRight.EndedDown)
		{
			ddP += Normalized(GameState->Camera.XAxis);
		}
		if(Input->KeyboardController.MoveLeft.EndedDown)
		{
			ddP -= Normalized(GameState->Camera.XAxis);
		}
		if(Input->KeyboardController.LeftShoulder.EndedDown)
		{
			ddP -= WorldY;
		}
		if(Input->KeyboardController.RightShoulder.EndedDown)
		{
			ddP += WorldY;
		}

		ddP *= CameraAcceleration;
		v3 DragForce = - CameraDragScale * GameState->dPCamera;
		ddP += DragForce;
		GameState->dPCamera += dt * ddP;
		v3 CameraDeltaP = dt * GameState->dPCamera + 0.5f * dt * dt * ddP;
		GameState->Camera.P += CameraDeltaP;

		if(Input->MouseButtons[PlatformMouseButton_Right].EndedDown)
		{
			if(!GameState->MouseDragging)
			{
				GameState->MouseDragging = true;
				GameState->MouseCacheP = V2(Input->MouseX, Input->MouseY);
			}

			if(GameState->MouseDragging)
			{
				v3 WorldX = V3(1.0f, 0.0f, 0.0f);
				v3 WorldY = V3(0.0f, 1.0f, 0.0f);
				v3 WorldZ = V3(0.0f, 0.0f, 1.0f);

				f32 Yaw = GetAngle(WorldX, GameState->Camera.XAxis, WorldY);
				mat4 YawRotation = Rotation(Yaw, WorldY);
				f32 Pitch = GetAngle((YawRotation * ToV4(WorldZ)).xyz, GameState->Camera.ZAxis, (YawRotation * ToV4(WorldX)).xyz);

				v2 CurrentMouse = V2(Input->MouseX, Input->MouseY);
				v2 DeltaMouse = GameState->MouseCacheP - CurrentMouse;

				GameState->MouseCacheP = CurrentMouse;

				f32 MouseSensitivity = 30.0f;
				f32 YawAcceleration = Radians(MouseSensitivity * DeltaMouse.x);
				f32 PitchAcceleration = Radians(MouseSensitivity * DeltaMouse.y);

				f32 YawPitchDrag = 10.0f;
				YawAcceleration += - YawPitchDrag * GameState->CameraYawSpeed;
				PitchAcceleration += - YawPitchDrag * GameState->CameraPitchSpeed;

				f32 SaveYawSpeed = GameState->CameraYawSpeed;
				f32 SavePitchSpeed = GameState->CameraPitchSpeed;
				GameState->CameraYawSpeed += YawAcceleration * dt;
				GameState->CameraPitchSpeed += PitchAcceleration * dt;

				Yaw += SaveYawSpeed * dt + 0.5f * dt * dt * YawAcceleration;
				if(Yaw > PI)
				{
					Yaw = Yaw - 2.0f * PI;
				}
				else if(Yaw < -PI)
				{
					Yaw = Yaw + 2.0f * PI;
				}

				Pitch += SavePitchSpeed * dt + 0.5f * dt * dt * PitchAcceleration;

				f32 CosYaw = Cos(Yaw);
				f32 SinYaw = Sin(Yaw);

				GameState->Camera.XAxis.x = CosYaw;
				GameState->Camera.XAxis.y = 0.0f;
				GameState->Camera.XAxis.z = -SinYaw;

				GameState->Camera.ZAxis.x = SinYaw;
				GameState->Camera.ZAxis.y = 0.0f;
				GameState->Camera.ZAxis.z = CosYaw;

				GameState->Camera.ZAxis = (Rotation(Pitch, GameState->Camera.XAxis) * ToV4(GameState->Camera.ZAxis)).xyz;
			}
		}
		else
		{
			GameState->MouseDragging = false;
		}
	}

	// NOTE(hugo): Updating Shadow Mapping Uniform Data
	// {
	shadow_mapping_ubo ShadowMappingUbo = {};
	v3 LightPosWS = V3(0.0f, 2.5f, 1.0f);
	mat4 LightMVP = VulkanPerspective(Radians(100.0f), 1.0f, 0.1f, 10.0f) * LookAt(LightPosWS, V3(1.0f, 0.0f, 0.0f), V3(0.0f, 0.0f, 1.0f)) * Scaling(0.01f * V3(1.0f, 1.0f, 1.0f));
	ShadowMappingUbo.LightMVP = LightMVP;
	VulkanSendMemoryToBuffer(Device, ShadowMappingUniformBuffer.Memory, sizeof(shadow_mapping_ubo), &ShadowMappingUbo);
	
	// }

	// NOTE(hugo): Updating Uniform Data
	// {

	// TODO(hugo): Use push constants here
	// which is a more efficient way to often
	// pass frequently chanding data to the shaders
	ubo CurrentUBO = {};
	mat4 ModelMatrix = Scaling(0.01f * V3(1.0f, 1.0f, 1.0f));
	mat4 ViewMatrix = LookAt(GameState->Camera.P, GameState->Camera.XAxis, GameState->Camera.ZAxis);

	float FoV = Radians(70);
	mat4 ProjectionMatrix = VulkanPerspective(FoV, float(GlobalWindowWidth) / float(GlobalWindowHeight), 0.1f, 50.0f);
	CurrentUBO.MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
	CurrentUBO.LightMVP = LightMVP;
	VulkanSendMemoryToBuffer(Device, UniformBuffer.Memory, sizeof(ubo), &CurrentUBO);

#if 0
	// NOTE(hugo): DEBUG
	{
		v4 VertexWorldSpace = V4(-0.5f, 0.5f, 0.0f, 1.0f);
		v4 VertexCameraSpace = ViewMatrix * VertexWorldSpace;
		v4 VertexProjectionSpace = ProjectionMatrix * VertexCameraSpace;
		v3 VertexClipSpace = VertexProjectionSpace.xyz / VertexProjectionSpace.w;
		int DummyBreak = 5;
		DummyBreak++;
	}
#endif
	// }

}

internal VkPipeline
VulkanCreateToneMappingPipeline(VkDevice Device,
		vk_shader_stage ShaderStage,
		VkPipelineLayout PipelineLayout,
		VkRenderPass RenderPass,
		u32 Width, u32 Height)
{
	VkPipeline ToneMappingPipeline = {};

	// NOTE(hugo): Pipeline specifics to shadow mapping
	VkVertexInputBindingDescription BindingDesc = {};
	BindingDesc.binding = 0;
	BindingDesc.stride = 2 * sizeof(v2);
	BindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// TODO(hugo): This depends on the vertex_layout_type selected
	VkVertexInputAttributeDescription AttributeDesc[2] = {};
	// NOTE(hugo): Position
	AttributeDesc[0].binding = 0;
	AttributeDesc[0].location = 0;
	AttributeDesc[0].format = VK_FORMAT_R32G32_SFLOAT;
	AttributeDesc[0].offset = 0;
	// NOTE(hugo): Tex coords
	AttributeDesc[1].binding = 0;
	AttributeDesc[1].location = 1;
	AttributeDesc[1].format = VK_FORMAT_R32G32_SFLOAT;
	AttributeDesc[1].offset = sizeof(v2);

	VkPipelineVertexInputStateCreateInfo VertexInputStateCreateInfo = {};
	VertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	VertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	VertexInputStateCreateInfo.pVertexBindingDescriptions = &BindingDesc;
	VertexInputStateCreateInfo.vertexAttributeDescriptionCount = ArrayCount(AttributeDesc);
	VertexInputStateCreateInfo.pVertexAttributeDescriptions = AttributeDesc;

	VkPipelineInputAssemblyStateCreateInfo InputAssemblyCreateInfo = {};
	InputAssemblyCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	InputAssemblyCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	InputAssemblyCreateInfo.primitiveRestartEnable = VK_FALSE;

	VkViewport Viewport = {};
	Viewport.x = 0.0f;
	Viewport.y = 0.0f;
	Viewport.width = Width;
	Viewport.height = Height;
	Viewport.minDepth = 0.0f;
	Viewport.maxDepth = 1.0f;

	VkRect2D Scissor = {};
	Scissor.offset = {0, 0};
	Scissor.extent = {Width, Height};

	VkPipelineViewportStateCreateInfo ViewportStateCreateInfo = {};
	ViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	ViewportStateCreateInfo.viewportCount = 1;
	ViewportStateCreateInfo.pViewports = &Viewport;
	ViewportStateCreateInfo.scissorCount = 1;
	ViewportStateCreateInfo.pScissors = &Scissor;

	VkPipelineRasterizationStateCreateInfo RasterizationStateCreateInfo = {};
	RasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	RasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
	RasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	RasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	RasterizationStateCreateInfo.lineWidth = 1.0f;
	RasterizationStateCreateInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
	RasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	RasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
	RasterizationStateCreateInfo.depthBiasConstantFactor = 0.0f;
	RasterizationStateCreateInfo.depthBiasClamp = 0.0f;
	RasterizationStateCreateInfo.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo MultisampleStateCreateInfo = {};
	MultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	MultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
	MultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	MultisampleStateCreateInfo.minSampleShading = 1.0f;
	MultisampleStateCreateInfo.pSampleMask = 0;
	MultisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
	MultisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

	// TODO(hugo): Alpha blending goes here
	VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = {};
	ColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
		VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	ColorBlendAttachmentState.blendEnable = VK_FALSE;
	ColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	ColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo ColorBlendStateCreateInfo = {};
	ColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	ColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
	ColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_COPY;
	ColorBlendStateCreateInfo.attachmentCount = 1;
	ColorBlendStateCreateInfo.pAttachments = &ColorBlendAttachmentState;
	ColorBlendStateCreateInfo.blendConstants[0] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[1] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[2] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[3] = 0.0f;

	VkPipelineDepthStencilStateCreateInfo DepthStencilStateCreateInfo = {};
	DepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	DepthStencilStateCreateInfo.pNext = 0;
	DepthStencilStateCreateInfo.flags = 0;
	DepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	DepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.front = DepthStencilStateCreateInfo.back;
	DepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	DepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.minDepthBounds = 0.0f;
	DepthStencilStateCreateInfo.maxDepthBounds = 1.0f;

	VkPipelineDynamicStateCreateInfo DynamicStateCreateInfo = {};
	DynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	DynamicStateCreateInfo.pNext = 0;
	DynamicStateCreateInfo.flags = 0;
	DynamicStateCreateInfo.dynamicStateCount = 0;
	DynamicStateCreateInfo.pDynamicStates = 0;

	// TODO(hugo): Maybe change the memory layout of the shader structure ???
	VkGraphicsPipelineCreateInfo GraphicsPipelineCreateInfo = {};
	GraphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	GraphicsPipelineCreateInfo.stageCount = 2;
	GraphicsPipelineCreateInfo.pStages = &ShaderStage.VertexCreateInfo;
	GraphicsPipelineCreateInfo.pVertexInputState = &VertexInputStateCreateInfo;
	GraphicsPipelineCreateInfo.pInputAssemblyState = &InputAssemblyCreateInfo;
	GraphicsPipelineCreateInfo.pViewportState = &ViewportStateCreateInfo;
	GraphicsPipelineCreateInfo.pRasterizationState = &RasterizationStateCreateInfo;
	GraphicsPipelineCreateInfo.pMultisampleState = &MultisampleStateCreateInfo;
	GraphicsPipelineCreateInfo.pDepthStencilState = &DepthStencilStateCreateInfo;
	GraphicsPipelineCreateInfo.pColorBlendState = &ColorBlendStateCreateInfo;
	GraphicsPipelineCreateInfo.pDynamicState = 0;
	GraphicsPipelineCreateInfo.layout = PipelineLayout;
	GraphicsPipelineCreateInfo.renderPass = RenderPass;
	GraphicsPipelineCreateInfo.subpass = 0;
	GraphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	GraphicsPipelineCreateInfo.basePipelineIndex = -1;
	GraphicsPipelineCreateInfo.flags = 0;

	VK_CHECK(vkCreateGraphicsPipelines(Device, VK_NULL_HANDLE, 1, &GraphicsPipelineCreateInfo, 0, &ToneMappingPipeline));

	return(ToneMappingPipeline);
}

internal VkPipeline
VulkanCreateShadowMappingPipeline(VkDevice Device,
		vk_shader_stage ShaderStage,
		VkPipelineLayout PipelineLayout,
		VkRenderPass RenderPass)
{
	VkPipeline ShadowMappingPipeline = {};

	// NOTE(hugo): Pipeline specifics to shadow mapping
	VkVertexInputBindingDescription BindingDesc = {};
	BindingDesc.binding = 0;
	BindingDesc.stride = sizeof(vertex);
	BindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// TODO(hugo): This depends on the vertex_layout_type selected
	VkVertexInputAttributeDescription AttributeDesc[3] = {};
	// NOTE(hugo): Position
	AttributeDesc[0].binding = 0;
	AttributeDesc[0].location = 0;
	AttributeDesc[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	AttributeDesc[0].offset = 0;
	// NOTE(hugo): Color
	AttributeDesc[1].binding = 0;
	AttributeDesc[1].location = 1;
	AttributeDesc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	AttributeDesc[1].offset = sizeof(v3);
	// NOTE(hugo): Texture Coordinates
	AttributeDesc[2].binding = 0;
	AttributeDesc[2].location = 2;
	AttributeDesc[2].format = VK_FORMAT_R32G32_SFLOAT;
	AttributeDesc[2].offset = 2 * sizeof(v3);

	VkPipelineVertexInputStateCreateInfo VertexInputStateCreateInfo = {};
	VertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	VertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	VertexInputStateCreateInfo.pVertexBindingDescriptions = &BindingDesc;
	VertexInputStateCreateInfo.vertexAttributeDescriptionCount = ArrayCount(AttributeDesc);
	VertexInputStateCreateInfo.pVertexAttributeDescriptions = AttributeDesc;

	VkPipelineInputAssemblyStateCreateInfo InputAssemblyCreateInfo = {};
	InputAssemblyCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	InputAssemblyCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	InputAssemblyCreateInfo.primitiveRestartEnable = VK_FALSE;

	VkViewport Viewport = {};
	Viewport.x = 0.0f;
	Viewport.y = 0.0f;
	Viewport.width = SHADOWMAP_DIM;
	Viewport.height = SHADOWMAP_DIM;
	Viewport.minDepth = 0.0f;
	Viewport.maxDepth = 1.0f;

	VkRect2D Scissor = {};
	Scissor.offset = {0, 0};
	Scissor.extent = {SHADOWMAP_DIM, SHADOWMAP_DIM};

	VkPipelineViewportStateCreateInfo ViewportStateCreateInfo = {};
	ViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	ViewportStateCreateInfo.viewportCount = 1;
	ViewportStateCreateInfo.pViewports = &Viewport;
	ViewportStateCreateInfo.scissorCount = 1;
	ViewportStateCreateInfo.pScissors = &Scissor;

	VkPipelineRasterizationStateCreateInfo RasterizationStateCreateInfo = {};
	RasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	RasterizationStateCreateInfo.depthClampEnable = VK_TRUE;
	RasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	RasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	RasterizationStateCreateInfo.lineWidth = 1.0f;
	RasterizationStateCreateInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
	RasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	RasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
	RasterizationStateCreateInfo.depthBiasConstantFactor = 0.0f;
	RasterizationStateCreateInfo.depthBiasClamp = 0.0f;
	RasterizationStateCreateInfo.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo MultisampleStateCreateInfo = {};
	MultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	MultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
	MultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	MultisampleStateCreateInfo.minSampleShading = 1.0f;
	MultisampleStateCreateInfo.pSampleMask = 0;
	MultisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
	MultisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

	// TODO(hugo): Alpha blending goes here
	VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = {};
	ColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
		VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	ColorBlendAttachmentState.blendEnable = VK_FALSE;
	ColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	ColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo ColorBlendStateCreateInfo = {};
	ColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	ColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
	ColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_COPY;
	ColorBlendStateCreateInfo.attachmentCount = 0;
	ColorBlendStateCreateInfo.pAttachments = 0;
	ColorBlendStateCreateInfo.blendConstants[0] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[1] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[2] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[3] = 0.0f;

	VkPipelineDepthStencilStateCreateInfo DepthStencilStateCreateInfo = {};
	DepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	DepthStencilStateCreateInfo.pNext = 0;
	DepthStencilStateCreateInfo.flags = 0;
	DepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	DepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.front = DepthStencilStateCreateInfo.back;
	DepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	DepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.minDepthBounds = 0.0f;
	DepthStencilStateCreateInfo.maxDepthBounds = 1.0f;

	VkDynamicState DynamicStates[] = {VK_DYNAMIC_STATE_DEPTH_BIAS};
	VkPipelineDynamicStateCreateInfo DynamicStateCreateInfo = {};
	DynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	DynamicStateCreateInfo.pNext = 0;
	DynamicStateCreateInfo.flags = 0;
	DynamicStateCreateInfo.dynamicStateCount = ArrayCount(DynamicStates);
	DynamicStateCreateInfo.pDynamicStates = DynamicStates;


	// TODO(hugo): Maybe change the memory layout of the shader structure ???
	VkGraphicsPipelineCreateInfo GraphicsPipelineCreateInfo = {};
	GraphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	GraphicsPipelineCreateInfo.stageCount = 2;
	GraphicsPipelineCreateInfo.pStages = &ShaderStage.VertexCreateInfo;
	GraphicsPipelineCreateInfo.pVertexInputState = &VertexInputStateCreateInfo;
	GraphicsPipelineCreateInfo.pInputAssemblyState = &InputAssemblyCreateInfo;
	GraphicsPipelineCreateInfo.pViewportState = &ViewportStateCreateInfo;
	GraphicsPipelineCreateInfo.pRasterizationState = &RasterizationStateCreateInfo;
	GraphicsPipelineCreateInfo.pMultisampleState = &MultisampleStateCreateInfo;
	GraphicsPipelineCreateInfo.pDepthStencilState = &DepthStencilStateCreateInfo;
	GraphicsPipelineCreateInfo.pColorBlendState = &ColorBlendStateCreateInfo;
	GraphicsPipelineCreateInfo.pDynamicState = &DynamicStateCreateInfo;
	GraphicsPipelineCreateInfo.layout = PipelineLayout;
	GraphicsPipelineCreateInfo.renderPass = RenderPass;
	GraphicsPipelineCreateInfo.subpass = 0;
	GraphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	GraphicsPipelineCreateInfo.basePipelineIndex = -1;
	GraphicsPipelineCreateInfo.flags = 0;

	VK_CHECK(vkCreateGraphicsPipelines(Device, VK_NULL_HANDLE, 1, &GraphicsPipelineCreateInfo, 0, &ShadowMappingPipeline));

	return(ShadowMappingPipeline);
}

int main(int ArgumentCout, char** Arguments)
{
	u32 SDLInitResult = SDL_Init(SDL_INIT_EVERYTHING);
	Assert(SDLInitResult == 0);

	SDL_version SDLVersionCompiled = {};
	SDL_version SDLVersionLinked = {};
	SDL_VERSION(&SDLVersionCompiled);
	SDL_GetVersion(&SDLVersionLinked);
	printf("SDL Compile Version: %d.%d.%d\n",
			SDLVersionCompiled.major, SDLVersionCompiled.minor, SDLVersionCompiled.patch);
	printf("SDL Link Version: %d.%d.%d\n",
			SDLVersionLinked.major, SDLVersionLinked.minor, SDLVersionLinked.patch);

	u32 WindowFlags = SDL_WINDOW_SHOWN;

	SDL_Window* Window = SDL_CreateWindow("SDL Vulkan Demo by Hugo Viala",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			GlobalWindowWidth, GlobalWindowHeight, WindowFlags);
	Assert(Window);

	// NOTE(hugo): Init multithreading
	sdl_thread_startup Startups[6] = {};
	platform_work_queue MultithreadingQueue = {};
	SDLMakeQueue(&MultithreadingQueue, ArrayCount(Startups), Startups);

#if VALIDATION_LAYER
	// NOTE(hugo): Check Validation Layer Support
	// {
	bool ValidationLayerSupported = true;
	char* ValidationLayers[] = {"VK_LAYER_LUNARG_standard_validation"};
	u32 LayerCount = 0;
	vkEnumerateInstanceLayerProperties(&LayerCount, 0);
	VkLayerProperties* AvailableLayers = AllocateArray(VkLayerProperties, LayerCount);
	vkEnumerateInstanceLayerProperties(&LayerCount, AvailableLayers);
	for(u32 ValLayerIndex = 0; ValLayerIndex < ArrayCount(ValidationLayers); ++ValLayerIndex)
	{
		char* ValidationLayerName = ValidationLayers[ValLayerIndex];
		bool LayerFound = false;
		for(u32 LayerIndex = 0; LayerIndex < LayerCount; ++LayerIndex)
		{
			VkLayerProperties* LayerProperties = AvailableLayers + LayerIndex;
			if(strcmp(ValidationLayerName, LayerProperties->layerName) == 0)
			{
				LayerFound = true;
			}
		}

		if(!LayerFound)
		{
			ValidationLayerSupported = false;
			break;
		}
	}
	Assert(ValidationLayerSupported);
	Free(AvailableLayers);
	// }
#endif

	// NOTE(hugo): Vulkan init
	VkApplicationInfo AppInfo = {};
	AppInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	AppInfo.pApplicationName = "Hello Triangle";
	AppInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	AppInfo.pEngineName = "tokiwa";
	AppInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	AppInfo.apiVersion = VK_API_VERSION_1_0;

	// NOTE(hugo) : Vulkan Create Instance
	// {
	VkInstanceCreateInfo InstanceCreateInfo = {};
	InstanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	InstanceCreateInfo.pApplicationInfo = &AppInfo;
	// TODO(hugo): How to get those values with SDL ?
#ifdef _WIN32
	const char* ExtensionNames[] = {VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME, VK_EXT_DEBUG_REPORT_EXTENSION_NAME};
	InstanceCreateInfo.enabledExtensionCount = ArrayCount(ExtensionNames);
	InstanceCreateInfo.ppEnabledExtensionNames = ExtensionNames;
#else
	const char* ExtensionNames[] = {VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_XCB_SURFACE_EXTENSION_NAME, VK_EXT_DEBUG_REPORT_EXTENSION_NAME};
	InstanceCreateInfo.enabledExtensionCount = ArrayCount(ExtensionNames);
	InstanceCreateInfo.ppEnabledExtensionNames = ExtensionNames;
#endif

#if VALIDATION_LAYER
	InstanceCreateInfo.enabledLayerCount = ArrayCount(ValidationLayers);
	InstanceCreateInfo.ppEnabledLayerNames = ValidationLayers;
#else
	InstanceCreateInfo.enabledLayerCount = 0;
#endif

	VkInstance Instance = {};
	VK_CHECK(vkCreateInstance(&InstanceCreateInfo, 0, &Instance));
	// }

#if VALIDATION_LAYER
	// NOTE(hugo): Setup Debug Callack
	// {
	PFN_vkCreateDebugReportCallbackEXT CreateDebugReport = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(Instance, "vkCreateDebugReportCallbackEXT");
	Assert(CreateDebugReport);

	PFN_vkDestroyDebugReportCallbackEXT DestroyDebugReport = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(Instance, "vkDestroyDebugReportCallbackEXT");
	Assert(DestroyDebugReport);

	PFN_vkDebugReportMessageEXT DebugReportMessage = (PFN_vkDebugReportMessageEXT) vkGetInstanceProcAddr(Instance, "vkDebugReportMessageEXT");
	Assert(DebugReportMessage);

	VkDebugReportCallbackCreateInfoEXT DebugReportCallbackCreateInfo = {};
	DebugReportCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	DebugReportCallbackCreateInfo.pNext = 0;
	DebugReportCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT |
		VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
	DebugReportCallbackCreateInfo.pfnCallback = ValidationDebugCallback;

    VkDebugReportCallbackEXT DebugReportCallback = {};
	VK_CHECK(CreateDebugReport(Instance, &DebugReportCallbackCreateInfo, 0, &DebugReportCallback));

	// }
#endif

	// NOTE(hugo): Vulkan Surface Creation
	// {
	VkSurfaceKHR Surface = {};

	SDL_SysWMinfo SDLWindowInfo = {};
	SDL_VERSION(&SDLWindowInfo.version);
	SDL_bool GetWindowWMInfoResult = SDL_GetWindowWMInfo(Window, &SDLWindowInfo);
	printf("%s\n", SDL_GetError());
	Assert(GetWindowWMInfoResult);

#ifdef _WIN32
	Assert(SDLWindowInfo.subsystem == SDL_SYSWM_WINDOWS);
	VkWin32SurfaceCreateInfoKHR SurfaceCreateInfo = {};
	SurfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	SurfaceCreateInfo.hwnd = SDLWindowInfo.info.win.window;
	SurfaceCreateInfo.hinstance = SDLWindowInfo.info.win.hinstance;

	vkCreateWin32SurfaceKHR(Instance, &SurfaceCreateInfo, 0, &Surface);
#else
	Assert(SDLWindowInfo.subsystem == SDL_SYSWM_X11);
	VkXcbSurfaceCreateInfoKHR SurfaceCreateInfo = {};
	SurfaceCreateInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	SurfaceCreateInfo.pNext = 0;
	SurfaceCreateInfo.flags = 0;
	SurfaceCreateInfo.connection = XGetXCBConnection(SDLWindowInfo.info.x11.display);
	SurfaceCreateInfo.window = SDLWindowInfo.info.x11.window;

	vkCreateXcbSurfaceKHR(Instance, &SurfaceCreateInfo, 0, &Surface);
#endif
	// }

	u32 ExtensionCount = 0;
	vkEnumerateInstanceExtensionProperties(0, &ExtensionCount, 0);
	printf("Vulkan Extension Count: %i\n", ExtensionCount);

	VkExtensionProperties* ExtensionProperties = AllocateArray(VkExtensionProperties, ExtensionCount);
	vkEnumerateInstanceExtensionProperties(0, &ExtensionCount, ExtensionProperties);

	for(u32 PropertyIndex = 0; PropertyIndex < ExtensionCount; ++PropertyIndex)
	{
		VkExtensionProperties* Property = ExtensionProperties + PropertyIndex;
		printf("%s\n", Property->extensionName);
	}
	Free(ExtensionProperties);

	// NOTE(hugo): Vulkan Pick Physical Device
	// {
	VkPhysicalDevice PhysicalDevice = VK_NULL_HANDLE;
	u32 DeviceCount = 0;
	vkEnumeratePhysicalDevices(Instance, &DeviceCount, 0);
	Assert(DeviceCount != 0);
	printf("Vulkan Physical Device Count: %i\n", DeviceCount);
	VkPhysicalDevice* PhysicalDevices = AllocateArray(VkPhysicalDevice, DeviceCount);
	vkEnumeratePhysicalDevices(Instance, &DeviceCount, PhysicalDevices);
	b32 DeviceFound = false;
	for(u32 DeviceIndex = 0; DeviceIndex < DeviceCount; ++DeviceIndex)
	{
		VkPhysicalDevice* TestPhysicalDevice = PhysicalDevices + DeviceIndex;
		VkPhysicalDeviceProperties DeviceProperties = {};
		vkGetPhysicalDeviceProperties(*TestPhysicalDevice, &DeviceProperties);
		printf("Device %i: %s\n", DeviceIndex, DeviceProperties.deviceName);

		// TODO(hugo): Select the best physical device
		if(DeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			PhysicalDevice = *TestPhysicalDevice;
			DeviceFound = true;
			break;
		}
	}
	Assert(DeviceFound);
	Free(PhysicalDevices);
	// }

	// NOTE(hugo): Listing extensions for the physical device picked
	// {
	u32 PhysicalDeviceExtensionCount = 0;
	vkEnumerateDeviceExtensionProperties(PhysicalDevice, 0, &PhysicalDeviceExtensionCount, 0);
	VkExtensionProperties* PhysicalDeviceExtensions = AllocateArray(VkExtensionProperties, PhysicalDeviceExtensionCount);
	vkEnumerateDeviceExtensionProperties(PhysicalDevice, 0, &PhysicalDeviceExtensionCount, PhysicalDeviceExtensions);
	b32 SwapchainExtensionFound = false;
	for(u32 ExtensionIndex = 0; ExtensionIndex < PhysicalDeviceExtensionCount; ++ExtensionIndex)
	{
		VkExtensionProperties* Extension = PhysicalDeviceExtensions + ExtensionIndex;
		printf("%s\n", Extension->extensionName);
		if(strcmp(Extension->extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0)
		{
			SwapchainExtensionFound = true;
		}
	}
	Assert(SwapchainExtensionFound);
	Free(PhysicalDeviceExtensions);
	// }

	u32 QueueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &QueueFamilyCount, 0);
	printf("Vulkan Queue Family Count: %i\n", QueueFamilyCount);
	VkQueueFamilyProperties* QueueFamilies = AllocateArray(VkQueueFamilyProperties, QueueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &QueueFamilyCount, QueueFamilies);

	u32 DesiredGraphicsQueueCount = 2;

	// NOTE(hugo): Specific Queue Family for graphics
#define VULKAN_INVALID_QUEUE_FAMILY_INDEX 0xFFFFFFFF
	u32 GraphicsQueueFamilyIndex = VULKAN_INVALID_QUEUE_FAMILY_INDEX;
	u32 PresentationQueueFamilyIndex = VULKAN_INVALID_QUEUE_FAMILY_INDEX;
	for(u32 QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyCount; ++QueueFamilyIndex)
	{
		VkQueueFamilyProperties* QueueFamilyProperties = QueueFamilies + QueueFamilyIndex;
		if((QueueFamilyProperties->queueCount > 0) &&
				(QueueFamilyProperties->queueFlags & VK_QUEUE_GRAPHICS_BIT))
		{
			Assert(QueueFamilyProperties->queueCount >= DesiredGraphicsQueueCount);
			GraphicsQueueFamilyIndex = QueueFamilyIndex;
		}

		VkBool32 PresentationSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(PhysicalDevice, QueueFamilyIndex, Surface, &PresentationSupport);
		if((QueueFamilyProperties->queueCount > 0) && PresentationSupport)
		{
			PresentationQueueFamilyIndex = QueueFamilyIndex;
		}

		if((GraphicsQueueFamilyIndex != VULKAN_INVALID_QUEUE_FAMILY_INDEX) &&
				(PresentationQueueFamilyIndex != VULKAN_INVALID_QUEUE_FAMILY_INDEX))
		{
			break;
		}
	}
	Assert(GraphicsQueueFamilyIndex != VULKAN_INVALID_QUEUE_FAMILY_INDEX);
	Assert(PresentationQueueFamilyIndex != VULKAN_INVALID_QUEUE_FAMILY_INDEX);
	printf("Vulkan Graphics Family Index: %i\n", GraphicsQueueFamilyIndex);
	printf("Vulkan Presentation Family Index: %i\n", PresentationQueueFamilyIndex);
	Free(QueueFamilies);

	float QueuePriority = 1.0f;
	VkDeviceQueueCreateInfo QueueCreateInfos[2] = {};
	QueueCreateInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	QueueCreateInfos[0].queueFamilyIndex = GraphicsQueueFamilyIndex;
	QueueCreateInfos[0].queueCount = DesiredGraphicsQueueCount;
	QueueCreateInfos[0].pQueuePriorities = &QueuePriority;

	QueueCreateInfos[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	QueueCreateInfos[1].queueFamilyIndex = PresentationQueueFamilyIndex;
	QueueCreateInfos[1].queueCount = 1;
	QueueCreateInfos[1].pQueuePriorities = &QueuePriority;

	VkPhysicalDeviceFeatures DeviceFeatures = {};
	vkGetPhysicalDeviceFeatures(PhysicalDevice, &DeviceFeatures);

	VkDeviceCreateInfo DeviceCreateInfo = {};
	DeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	// TODO(hugo): Maybe not the perfect code to push only one queue if the indices are the same...
	DeviceCreateInfo.queueCreateInfoCount = (GraphicsQueueFamilyIndex == PresentationQueueFamilyIndex) ? 1 : ArrayCount(QueueCreateInfos);
	DeviceCreateInfo.pQueueCreateInfos = QueueCreateInfos;
	DeviceCreateInfo.enabledLayerCount = 0;
	DeviceCreateInfo.pEnabledFeatures = &DeviceFeatures;
	const char* DeviceExtensionNames[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
	DeviceCreateInfo.enabledExtensionCount = ArrayCount(DeviceExtensionNames);
	DeviceCreateInfo.ppEnabledExtensionNames = DeviceExtensionNames;

	VkDevice Device = {};
	VK_CHECK(vkCreateDevice(PhysicalDevice, &DeviceCreateInfo, 0, &Device));

	VkQueue* GraphicsQueues = AllocateArray(VkQueue, DesiredGraphicsQueueCount);
	vkGetDeviceQueue(Device, GraphicsQueueFamilyIndex, 0, GraphicsQueues + 0);
	vkGetDeviceQueue(Device, GraphicsQueueFamilyIndex, 1, GraphicsQueues + 1);
	VkQueue PresentationQueue = {};
	vkGetDeviceQueue(Device, PresentationQueueFamilyIndex, 0, &PresentationQueue);


	// NOTE(hugo): Querying swapchain support
	// {
	VkSurfaceCapabilitiesKHR SurfaceCaps = {};
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDevice, Surface, &SurfaceCaps);

	u32 SurfaceFormatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDevice, Surface, &SurfaceFormatCount, 0);
	printf("Vulkan Format Count: %i\n", SurfaceFormatCount);
	Assert(SurfaceFormatCount != 0);
	VkSurfaceFormatKHR* SurfaceFormats = AllocateArray(VkSurfaceFormatKHR, SurfaceFormatCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDevice, Surface, &SurfaceFormatCount, SurfaceFormats);

#if 0
	VkPresentModeKHR* PresentModes = AllocateArray(VkPresentModeKHR, SurfaceFormatCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(PhysicalDevice, Surface, &SurfaceFormatCount, PresentModes);
	Free(PresentModes);
#endif

	VkSurfaceFormatKHR SelectedSurfaceFormat = {};
	if((SurfaceFormatCount == 1) && (SurfaceFormats[0].format == VK_FORMAT_UNDEFINED))
	{
		SelectedSurfaceFormat = {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
	}
	else
	{
		b32 FormatFound = false;
		for(u32 FormatIndex = 0; FormatIndex < SurfaceFormatCount; ++FormatIndex)
		{
			VkSurfaceFormatKHR* TestSurface = SurfaceFormats + FormatIndex;
			if((TestSurface->format == VK_FORMAT_B8G8R8A8_UNORM) && (TestSurface->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR))
			{
				SelectedSurfaceFormat = *TestSurface;
				FormatFound = true;
				break;
			}
		}
		Assert(FormatFound);
	}
	Free(SurfaceFormats);

	// TODO(hugo): Select a better presentation mode
	VkPresentModeKHR SelectedPresentMode = VK_PRESENT_MODE_FIFO_KHR;
	VkExtent2D SelectedExtent = {GlobalWindowWidth, GlobalWindowHeight};

	u32 SwapchainImageCount = SurfaceCaps.minImageCount + 1;
	if((SurfaceCaps.maxImageCount > 0) && (SwapchainImageCount > SurfaceCaps.maxImageCount))
	{
		SwapchainImageCount = SurfaceCaps.maxImageCount;
	}

	VkSwapchainCreateInfoKHR SwapchainCreateInfo = {};
	SwapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	SwapchainCreateInfo.surface = Surface;
	SwapchainCreateInfo.minImageCount = SwapchainImageCount;
	SwapchainCreateInfo.imageFormat = SelectedSurfaceFormat.format;
	SwapchainCreateInfo.imageColorSpace = SelectedSurfaceFormat.colorSpace;
	SwapchainCreateInfo.imageExtent = SelectedExtent;
	SwapchainCreateInfo.imageArrayLayers = 1;
	// TODO(hugo): Change the image usage for post-processing
	SwapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if(GraphicsQueueFamilyIndex == PresentationQueueFamilyIndex)
	{
		SwapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		SwapchainCreateInfo.queueFamilyIndexCount = 0;
		SwapchainCreateInfo.pQueueFamilyIndices = 0;
	}
	else
	{
		u32 QueueFamilyIndices[] = {GraphicsQueueFamilyIndex, PresentationQueueFamilyIndex};
		SwapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		SwapchainCreateInfo.queueFamilyIndexCount = ArrayCount(QueueFamilyIndices);
		SwapchainCreateInfo.pQueueFamilyIndices = QueueFamilyIndices;
	}
	SwapchainCreateInfo.preTransform = SurfaceCaps.currentTransform;
	SwapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	SwapchainCreateInfo.presentMode = SelectedPresentMode;
	SwapchainCreateInfo.clipped = VK_TRUE;
	SwapchainCreateInfo.oldSwapchain = 0;

	VkSwapchainKHR Swapchain = {};
	VK_CHECK(vkCreateSwapchainKHR(Device, &SwapchainCreateInfo, 0, &Swapchain));
	// }

	// NOTE(hugo): Retrieving the swapchain images
	// {
	u32 ImageCount = 0;
	vkGetSwapchainImagesKHR(Device, Swapchain, &ImageCount, 0);
	printf("Vulkan Image View Count: %i\n", ImageCount);
	VkImage* SwapchainImages = AllocateArray(VkImage, ImageCount);
	vkGetSwapchainImagesKHR(Device, Swapchain, &ImageCount, SwapchainImages);
	// }
	
	// NOTE(hugo): Creating the image views
	// {
	VkImageView* SwapchainImageViews = AllocateArray(VkImageView, ImageCount);
	for(u32 ImageViewIndex = 0; ImageViewIndex < ImageCount; ++ImageViewIndex)
	{
		SwapchainImageViews[ImageViewIndex] = VulkanCreateSimpleImageView(Device,
				SwapchainImages[ImageViewIndex], SelectedSurfaceFormat.format,
				VK_IMAGE_ASPECT_COLOR_BIT);
	}
	Free(SwapchainImages);
	// }

	// NOTE(hugo): Render Pass Creation
	// {

	VkRenderPass CompositeRenderPass = {};
	{
		VkAttachmentDescription ColorAttachmentDescription = {};
		//ColorAttachmentDescription.format = VK_FORMAT_B8G8R8A8_UNORM;
		ColorAttachmentDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
		ColorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT; // TODO(hugo): Multisampling here !!
		ColorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		ColorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		ColorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		ColorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		ColorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		ColorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		//ColorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference ColorAttachmentReference = {};
		ColorAttachmentReference.attachment = 0;
		ColorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		// NOTE(hugo): Depth Attachment
		// {
		VkAttachmentDescription DepthAttachmentDescription = {};
		DepthAttachmentDescription.format = VULKAN_DEPTH_FORMAT;
		DepthAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
		DepthAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		DepthAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		DepthAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		DepthAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		DepthAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		DepthAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference DepthAttachmentReference = {};
		DepthAttachmentReference.attachment = 1;
		DepthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		// }

		VkSubpassDescription SubpassDescription = {};
		SubpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		SubpassDescription.colorAttachmentCount = 1;
		SubpassDescription.pColorAttachments = &ColorAttachmentReference;
		SubpassDescription.pDepthStencilAttachment = &DepthAttachmentReference;

		// TODO(hugo): Better subpass dep
#if 0
		Assert(!"wrong dep");
		Assert(!"do not forget to put the proper dependency count just below when reactivating that code");
		VkSubpassDependency SubpassDependencies[2] = {};
		SubpassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		SubpassDependencies[0].dstSubpass = 0;
		SubpassDependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		SubpassDependencies[0].dstStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		SubpassDependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		SubpassDependencies[0].dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		SubpassDependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		SubpassDependencies[1].srcSubpass = 0;
		SubpassDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
		SubpassDependencies[1].srcStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		SubpassDependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		SubpassDependencies[1].srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		SubpassDependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		SubpassDependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
#endif

		VkAttachmentDescription AttachmentDescriptions[] = {ColorAttachmentDescription, DepthAttachmentDescription};
		VkRenderPassCreateInfo RenderPassCreateInfo = {};
		RenderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		RenderPassCreateInfo.attachmentCount = ArrayCount(AttachmentDescriptions);
		RenderPassCreateInfo.pAttachments = AttachmentDescriptions;
		RenderPassCreateInfo.subpassCount = 1;
		RenderPassCreateInfo.pSubpasses = &SubpassDescription;
#if 0
		RenderPassCreateInfo.dependencyCount = ArrayCount(SubpassDependencies);
		RenderPassCreateInfo.pDependencies = SubpassDependencies;
#else
		RenderPassCreateInfo.dependencyCount = 0;
		RenderPassCreateInfo.pDependencies = 0;
#endif

		VK_CHECK(vkCreateRenderPass(Device, &RenderPassCreateInfo, 0, &CompositeRenderPass));
	}
	
	// }

	// NOTE(hugo): Descriptor Set Layout Creation
	// {
	VkDescriptorSetLayoutBinding UBOLayoutBinding = {};
	UBOLayoutBinding.binding = 0;
	UBOLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	UBOLayoutBinding.descriptorCount = 1;
	UBOLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	UBOLayoutBinding.pImmutableSamplers = 0;
	// }

	// NOTE(hugo): Descriptor Binding for Sampler
	// {
	VkDescriptorSetLayoutBinding SamplerLayoutBinding = {};
	SamplerLayoutBinding.binding = 1; // TODO(hugo): Automatic increment binding index
	SamplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	SamplerLayoutBinding.descriptorCount = 1;
	SamplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	SamplerLayoutBinding.pImmutableSamplers = 0;
	// }

	// NOTE(hugo): Descriptor Binding for Shadow Map Sampler
	// {
	VkDescriptorSetLayoutBinding ShadowMapSamplerLayoutBinding = {};
	ShadowMapSamplerLayoutBinding.binding = 2; // TODO(hugo): Automatic increment binding index
	ShadowMapSamplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	ShadowMapSamplerLayoutBinding.descriptorCount = 1;
	ShadowMapSamplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	ShadowMapSamplerLayoutBinding.pImmutableSamplers = 0;
	// }

	VkDescriptorSetLayoutBinding LayoutBindings[] = {UBOLayoutBinding, SamplerLayoutBinding, ShadowMapSamplerLayoutBinding};
	VkDescriptorSetLayoutCreateInfo UniformLayoutCreateInfo = {};
	UniformLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	UniformLayoutCreateInfo.bindingCount = ArrayCount(LayoutBindings);
	UniformLayoutCreateInfo.pBindings = LayoutBindings;

	VkDescriptorSetLayout UniformLayout = {};
	VK_CHECK(vkCreateDescriptorSetLayout(Device, &UniformLayoutCreateInfo, 0, &UniformLayout));
	
	// NOTE(hugo): Uniform Buffer Creation
	// {
	VkDeviceSize UniformBufferSize = sizeof(ubo);
	vk_buffer UniformBuffer = VulkanCreateBuffer(Device, PhysicalDevice,
			UniformBufferSize,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	// }

	// NOTE(hugo): Graphics Pipeline Creation
	// {
	vk_shader_stage ShaderStage = VulkanCreateShaderStage(Device, "triangle.vert.spv", "triangle.frag.spv");

	VkVertexInputBindingDescription BindingDesc = {};
	BindingDesc.binding = 0;
	BindingDesc.stride = sizeof(vertex);
	BindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// TODO(hugo): This depends on the vertex_layout_type selected
	VkVertexInputAttributeDescription AttributeDesc[3] = {};
	// NOTE(hugo): Position
	AttributeDesc[0].binding = 0;
	AttributeDesc[0].location = 0;
	AttributeDesc[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	AttributeDesc[0].offset = 0;
	// NOTE(hugo): Color
	AttributeDesc[1].binding = 0;
	AttributeDesc[1].location = 1;
	AttributeDesc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	AttributeDesc[1].offset = sizeof(v3);
	// NOTE(hugo): Texture Coordinates
	AttributeDesc[2].binding = 0;
	AttributeDesc[2].location = 2;
	AttributeDesc[2].format = VK_FORMAT_R32G32_SFLOAT;
	AttributeDesc[2].offset = 2 * sizeof(v3);

	VkPipelineVertexInputStateCreateInfo VertexInputStateCreateInfo = {};
	VertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	VertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	VertexInputStateCreateInfo.pVertexBindingDescriptions = &BindingDesc;
	VertexInputStateCreateInfo.vertexAttributeDescriptionCount = ArrayCount(AttributeDesc);
	VertexInputStateCreateInfo.pVertexAttributeDescriptions = AttributeDesc;

	VkPipelineInputAssemblyStateCreateInfo InputAssemblyCreateInfo = {};
	InputAssemblyCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	InputAssemblyCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	InputAssemblyCreateInfo.primitiveRestartEnable = VK_FALSE;
	
	VkViewport Viewport = {};
	Viewport.x = 0.0f;
	Viewport.y = 0.0f;
	Viewport.width = float(SelectedExtent.width);
	Viewport.height = float(SelectedExtent.height);
	Viewport.minDepth = 0.0f;
	Viewport.maxDepth = 1.0f;

	VkRect2D Scissor = {};
	Scissor.offset = {0, 0};
	Scissor.extent = SelectedExtent;

	VkPipelineViewportStateCreateInfo ViewportStateCreateInfo = {};
	ViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	ViewportStateCreateInfo.viewportCount = 1;
	ViewportStateCreateInfo.pViewports = &Viewport;
	ViewportStateCreateInfo.scissorCount = 1;
	ViewportStateCreateInfo.pScissors = &Scissor;

	VkPipelineRasterizationStateCreateInfo RasterizationStateCreateInfo = {};
	RasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	RasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
	RasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	RasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	RasterizationStateCreateInfo.lineWidth = 1.0f;
	RasterizationStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	RasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	RasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
	RasterizationStateCreateInfo.depthBiasConstantFactor = 0.0f;
	RasterizationStateCreateInfo.depthBiasClamp = 0.0f;
	RasterizationStateCreateInfo.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo MultisampleStateCreateInfo = {};
	MultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	MultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
	MultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	MultisampleStateCreateInfo.minSampleShading = 1.0f;
	MultisampleStateCreateInfo.pSampleMask = 0;
	MultisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
	MultisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

	// TODO(hugo): Alpha blending goes here
	VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = {};
	ColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
		VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	ColorBlendAttachmentState.blendEnable = VK_FALSE;
	ColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	ColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	ColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo ColorBlendStateCreateInfo = {};
	ColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	ColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
	ColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_COPY;
	ColorBlendStateCreateInfo.attachmentCount = 1;
	ColorBlendStateCreateInfo.pAttachments = &ColorBlendAttachmentState;
	ColorBlendStateCreateInfo.blendConstants[0] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[1] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[2] = 0.0f;
	ColorBlendStateCreateInfo.blendConstants[3] = 0.0f;

	VkPipelineLayoutCreateInfo LayoutCreateInfo = {};
	LayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	LayoutCreateInfo.setLayoutCount = 1;
	LayoutCreateInfo.pSetLayouts = &UniformLayout;
	LayoutCreateInfo.pushConstantRangeCount = 0;
	LayoutCreateInfo.pPushConstantRanges = 0;

	VkPipelineLayout CompositePipelineLayout = {};
	VK_CHECK(vkCreatePipelineLayout(Device, &LayoutCreateInfo, 0, &CompositePipelineLayout));

	VkPipelineDepthStencilStateCreateInfo DepthStencilStateCreateInfo = {};
	DepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	DepthStencilStateCreateInfo.pNext = 0;
	DepthStencilStateCreateInfo.flags = 0;
	DepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	DepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	DepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.front = DepthStencilStateCreateInfo.back;
	DepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	DepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	DepthStencilStateCreateInfo.minDepthBounds = 0.0f;
	DepthStencilStateCreateInfo.maxDepthBounds = 1.0f;

	// NOTE(hugo): FINALLY

	// TODO(hugo): Maybe change the memory layout of the shader structure ???
	VkGraphicsPipelineCreateInfo GraphicsPipelineCreateInfo = {};
	GraphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	GraphicsPipelineCreateInfo.stageCount = 2;
	GraphicsPipelineCreateInfo.pStages = &ShaderStage.VertexCreateInfo;
	GraphicsPipelineCreateInfo.pVertexInputState = &VertexInputStateCreateInfo;
	GraphicsPipelineCreateInfo.pInputAssemblyState = &InputAssemblyCreateInfo;
	GraphicsPipelineCreateInfo.pViewportState = &ViewportStateCreateInfo;
	GraphicsPipelineCreateInfo.pRasterizationState = &RasterizationStateCreateInfo;
	GraphicsPipelineCreateInfo.pMultisampleState = &MultisampleStateCreateInfo;
	GraphicsPipelineCreateInfo.pDepthStencilState = &DepthStencilStateCreateInfo;
	GraphicsPipelineCreateInfo.pColorBlendState = &ColorBlendStateCreateInfo;
	GraphicsPipelineCreateInfo.pDynamicState = 0;
	GraphicsPipelineCreateInfo.layout = CompositePipelineLayout;
	GraphicsPipelineCreateInfo.renderPass = CompositeRenderPass;
	GraphicsPipelineCreateInfo.subpass = 0;
	GraphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	GraphicsPipelineCreateInfo.basePipelineIndex = -1;
	GraphicsPipelineCreateInfo.flags = 0;

	VkPipeline CompositeGraphicsPipeline = {};
	VK_CHECK(vkCreateGraphicsPipelines(Device, VK_NULL_HANDLE, 1, &GraphicsPipelineCreateInfo, 0, &CompositeGraphicsPipeline));
	// }

	// NOTE(hugo): Shadow map offscreen render pass
	// {
	VkRenderPass ShadowMappingRenderPass = {};
	{
		VkAttachmentDescription DepthAttachmentDescription = {};
		DepthAttachmentDescription.format = VULKAN_DEPTH_FORMAT;
		DepthAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
		DepthAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		DepthAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		DepthAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		DepthAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		// TODO(hugo): Shouldn't the initial layout also be depth/stencil attachment optimal ?
		DepthAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		DepthAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference DepthAttachmentReference = {};
		DepthAttachmentReference.attachment = 0;
		DepthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkSubpassDescription SubpassDescription = {};
		SubpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		SubpassDescription.colorAttachmentCount = 0;
		SubpassDescription.pColorAttachments = 0;
		SubpassDescription.pDepthStencilAttachment = &DepthAttachmentReference;

#if 0
		VkSubpassDependency SubpassDependencies[2] = {};
		SubpassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		SubpassDependencies[0].dstSubpass = 0;
		SubpassDependencies[0].srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		SubpassDependencies[0].dstStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		SubpassDependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		SubpassDependencies[0].dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		SubpassDependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		SubpassDependencies[1].srcSubpass = 0;
		SubpassDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
		SubpassDependencies[1].srcStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		SubpassDependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		SubpassDependencies[1].srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		SubpassDependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		SubpassDependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
#endif

		VkViewport Viewport = {};
		Viewport.x = 0.0f;
		Viewport.y = 0.0f;
		Viewport.width = SHADOWMAP_DIM;
		Viewport.height = SHADOWMAP_DIM;
		Viewport.minDepth = 0.0f;
		Viewport.maxDepth = 1.0f;

		VkRect2D Scissor = {};
		Scissor.offset = {0, 0};
		Scissor.extent = {SHADOWMAP_DIM, SHADOWMAP_DIM};

		ViewportStateCreateInfo.viewportCount = 1;
		ViewportStateCreateInfo.pViewports = &Viewport;
		ViewportStateCreateInfo.scissorCount = 1;
		ViewportStateCreateInfo.pScissors = &Scissor;

		VkRenderPassCreateInfo RenderPassCreateInfo = {};
		RenderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		RenderPassCreateInfo.attachmentCount = 1;
		RenderPassCreateInfo.pAttachments = &DepthAttachmentDescription;
		RenderPassCreateInfo.subpassCount = 1;
		RenderPassCreateInfo.pSubpasses = &SubpassDescription;
#if 0
		RenderPassCreateInfo.dependencyCount = ArrayCount(SubpassDependencies);
		RenderPassCreateInfo.pDependencies = SubpassDependencies;
#else
		RenderPassCreateInfo.dependencyCount = 0;
		RenderPassCreateInfo.pDependencies = 0;
#endif

		VK_CHECK(vkCreateRenderPass(Device, &RenderPassCreateInfo, 0, &ShadowMappingRenderPass));
	}
	// }

	// NOTE(hugo): ToneMapping render pass creation
	// {
	VkRenderPass ToneMappingRenderPass = {};
	{
		VkAttachmentReference ColorAttachmentReference = {};
		ColorAttachmentReference.attachment = 0;
		ColorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkSubpassDescription SubpassDescription = {};
		SubpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		SubpassDescription.colorAttachmentCount = 1;
		SubpassDescription.pColorAttachments = &ColorAttachmentReference;
		SubpassDescription.pDepthStencilAttachment = 0;
		// TODO(hugo): Subpass dependency

		VkViewport Viewport = {};
		Viewport.x = 0.0f;
		Viewport.y = 0.0f;
		Viewport.width = SelectedExtent.width;
		Viewport.height = SelectedExtent.height;
		Viewport.minDepth = 0.0f;
		Viewport.maxDepth = 1.0f;

		VkRect2D Scissor = {};
		Scissor.offset = {0, 0};
		Scissor.extent = {SelectedExtent.width, SelectedExtent.height};

		ViewportStateCreateInfo.viewportCount = 1;
		ViewportStateCreateInfo.pViewports = &Viewport;
		ViewportStateCreateInfo.scissorCount = 1;
		ViewportStateCreateInfo.pScissors = &Scissor;

		VkAttachmentDescription ColorAttachmentDescription = {};
		ColorAttachmentDescription.format = SelectedSurfaceFormat.format;
		ColorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT; // TODO(hugo): Multisampling here !!
		ColorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		ColorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		ColorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		ColorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		ColorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		ColorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkRenderPassCreateInfo RenderPassCreateInfo = {};
		RenderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		RenderPassCreateInfo.attachmentCount = 1;
		RenderPassCreateInfo.pAttachments = &ColorAttachmentDescription;
		RenderPassCreateInfo.subpassCount = 1;
		RenderPassCreateInfo.pSubpasses = &SubpassDescription;
		RenderPassCreateInfo.dependencyCount = 0;
		RenderPassCreateInfo.pDependencies = 0;

		VK_CHECK(vkCreateRenderPass(Device, &RenderPassCreateInfo, 0, &ToneMappingRenderPass));
	}
	// }

	// NOTE(hugo): Composite render pass framebuffer creation
	// {
	vk_attachment CompositePassColorAttachment = VulkanCreateAttachment2D(Device, PhysicalDevice,
			GlobalWindowWidth, GlobalWindowHeight, VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
	vk_attachment CompositePassDepthAttachment = VulkanCreateAttachment2D(Device, PhysicalDevice,
			GlobalWindowWidth, GlobalWindowHeight, VULKAN_DEPTH_FORMAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

	VkImageView CompositePassAttachmentViews[2] = {CompositePassColorAttachment.ImageView, CompositePassDepthAttachment.ImageView};
	VkFramebuffer CompositePassFramebuffer = VulkanCreateFramebuffer(Device, CompositePassAttachmentViews, 2,
			CompositeRenderPass, GlobalWindowWidth, GlobalWindowHeight);
	// }

	// NOTE(hugo): Shadow mapping framebuffer creation
	// {
	vk_attachment ShadowMappingAttachment = VulkanCreateAttachment2D(Device, PhysicalDevice,
			SHADOWMAP_DIM, SHADOWMAP_DIM, VULKAN_DEPTH_FORMAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

	VkFramebuffer ShadowMappingFramebuffer =
		 VulkanCreateFramebuffer(Device, &ShadowMappingAttachment.ImageView, 1,
			ShadowMappingRenderPass, SHADOWMAP_DIM, SHADOWMAP_DIM);

	VkSampler ShadowMappingSampler = {};
	{
		VkSamplerCreateInfo SamplerCreateInfo = {};
		SamplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		SamplerCreateInfo.magFilter = VK_FILTER_LINEAR;
		SamplerCreateInfo.minFilter = VK_FILTER_LINEAR;
		SamplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.anisotropyEnable = VK_FALSE;
		SamplerCreateInfo.maxAnisotropy = 1;
		// NOTE(hugo): Here a diff with the other sampler
		SamplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		SamplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
		SamplerCreateInfo.compareEnable = VK_FALSE;
		SamplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		SamplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		SamplerCreateInfo.mipLodBias = 0.0f;
		SamplerCreateInfo.minLod = 0.0f;
		SamplerCreateInfo.maxLod = 0.0f;

		VK_CHECK(vkCreateSampler(Device, &SamplerCreateInfo, 0, &ShadowMappingSampler));

	}
	// }

	// NOTE(hugo): Tone Mapping Sampler Create
	// {
	VkSampler ToneMappingSampler = {};
	{
		VkSamplerCreateInfo SamplerCreateInfo = {};
		SamplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		SamplerCreateInfo.magFilter = VK_FILTER_LINEAR;
		SamplerCreateInfo.minFilter = VK_FILTER_LINEAR;
		SamplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		SamplerCreateInfo.anisotropyEnable = VK_FALSE;
		SamplerCreateInfo.maxAnisotropy = 1;
		SamplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		SamplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
		SamplerCreateInfo.compareEnable = VK_FALSE;
		SamplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		SamplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		SamplerCreateInfo.mipLodBias = 0.0f;
		SamplerCreateInfo.minLod = 0.0f;
		SamplerCreateInfo.maxLod = 0.0f;

		VK_CHECK(vkCreateSampler(Device, &SamplerCreateInfo, 0, &ToneMappingSampler));

	}
	// }

	// NOTE(hugo): Command Pool Creation
	// {
	VkCommandPoolCreateInfo CommandPoolCreateInfo = {};
	CommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	CommandPoolCreateInfo.queueFamilyIndex = GraphicsQueueFamilyIndex;
	CommandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	VkCommandPool CommandPool = {};
	VK_CHECK(vkCreateCommandPool(Device, &CommandPoolCreateInfo, 0, &CommandPool));
	// }

	// NOTE(hugo): Depth Resource Creation
	// {
	VkImageCreateInfo DepthImageCreateInfo = {};
	DepthImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	DepthImageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	DepthImageCreateInfo.extent.width = SelectedExtent.width;
	DepthImageCreateInfo.extent.height = SelectedExtent.height;
	DepthImageCreateInfo.extent.depth = 1;
	DepthImageCreateInfo.mipLevels = 1;
	DepthImageCreateInfo.arrayLayers = 1;
	DepthImageCreateInfo.format = VULKAN_DEPTH_FORMAT;
	DepthImageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	DepthImageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	DepthImageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	DepthImageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	DepthImageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	DepthImageCreateInfo.flags = 0;

#if 0
	VkImage DepthImage = {};
	VK_CHECK(vkCreateImage(Device, &DepthImageCreateInfo, 0, &DepthImage));

	VkDeviceMemory DepthImageMemory = VulkanAllocateImageMemory(Device, PhysicalDevice, DepthImage);
	VK_CHECK(vkBindImageMemory(Device, DepthImage, DepthImageMemory, 0));

	VkImageView DepthImageView = VulkanCreateSimpleImageView(Device,
			DepthImage, VULKAN_DEPTH_FORMAT,
			VK_IMAGE_ASPECT_DEPTH_BIT);

	VulkanTransitionImageLayout(Device, GraphicsQueues[0], CommandPool, DepthImage,
			VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
			VulkanImageTransitionLayoutType_UndefToDepthStencilOptimal);
#endif

	// }
	
	// NOTE(hugo): Framebuffer setup
	// {
	VkFramebuffer* SwapchainFramebuffers = AllocateArray(VkFramebuffer, ImageCount);
	for(u32 ImageIndex = 0; ImageIndex < SwapchainImageCount; ++ImageIndex)
	{
#if 1
		VkImageView Attachments[] = {SwapchainImageViews[ImageIndex]};
		SwapchainFramebuffers[ImageIndex] = VulkanCreateFramebuffer(Device,
				Attachments, ArrayCount(Attachments), ToneMappingRenderPass,
				SelectedExtent.width, SelectedExtent.height);
#else
		VkImageView Attachments[] = {SwapchainImageViews[ImageIndex], DepthImageView};
		SwapchainFramebuffers[ImageIndex] = VulkanCreateFramebuffer(Device,
				Attachments, ArrayCount(Attachments), CompositeRenderPass,
				SelectedExtent.width, SelectedExtent.height);
#endif
	}
	// }

	mesh_list MeshList = {};
	texture_list TextureList = {};
	vk_mesh_buffer MeshBuffers[512] = {};
	char* Filename = "sponza_mod.msh";

	VkDescriptorBufferInfo DescriptorBufferInfo = {};
	DescriptorBufferInfo.buffer = UniformBuffer.Buffer;
	DescriptorBufferInfo.offset = 0;
	DescriptorBufferInfo.range = sizeof(ubo);

	VkDescriptorPool DescriptorPool = {};

	VkDescriptorSet DescriptorSets[512] = {};

	// NOTE(hugo): Create Texture Sampler
	// {
	VkSamplerCreateInfo SamplerCreateInfo = {};
	SamplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	SamplerCreateInfo.magFilter = VK_FILTER_LINEAR;
	SamplerCreateInfo.minFilter = VK_FILTER_LINEAR;
	SamplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	SamplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	SamplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
#if 0
	SamplerCreateInfo.anisotropyEnable = VK_TRUE;
	SamplerCreateInfo.maxAnisotropy = 16;
#else
	SamplerCreateInfo.anisotropyEnable = VK_FALSE;
	SamplerCreateInfo.maxAnisotropy = 1;
#endif
	SamplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	SamplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
	SamplerCreateInfo.compareEnable = VK_FALSE;
	SamplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	SamplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	SamplerCreateInfo.mipLodBias = 0.0f;
	SamplerCreateInfo.minLod = 0.0f;
	SamplerCreateInfo.maxLod = 0.0f;

	VkSampler TextureSampler = {};
	VK_CHECK(vkCreateSampler(Device, &SamplerCreateInfo, 0, &TextureSampler));

	// }

	vk_attachment ImageTextures[512] = {};
	VkDescriptorImageInfo DescriptorImageInfos[512] = {};

	// NOTE(hugo) : Shadow Mapping Descriptor Map Sampler
	// {
	VkDescriptorImageInfo ShadowMappingDescriptorImageInfo = {};
	ShadowMappingDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	ShadowMappingDescriptorImageInfo.imageView = ShadowMappingAttachment.ImageView;
	ShadowMappingDescriptorImageInfo.sampler = ShadowMappingSampler;
	// }


	load_mesh_data LoadMeshData = {};
	LoadMeshData.MeshList = &MeshList;
	LoadMeshData.MeshBuffers = MeshBuffers;
	LoadMeshData.TextureList = &TextureList;
	LoadMeshData.Filename = Filename;
	LoadMeshData.Device = Device;
	LoadMeshData.PhysicalDevice = PhysicalDevice;
	// TODO(hugo): Set another command pool that will only be used in the other thread
	// (is it possible ?)
	LoadMeshData.GraphicsQueue = GraphicsQueues[1];
	LoadMeshData.DescriptorBufferInfo = &DescriptorBufferInfo;
	LoadMeshData.DescriptorSets = DescriptorSets;
	LoadMeshData.DescriptorImageInfos = DescriptorImageInfos;
	LoadMeshData.ShadowMappingDescriptorImageInfo = &ShadowMappingDescriptorImageInfo;
	LoadMeshData.TextureSampler = &TextureSampler;
	LoadMeshData.ImageTextures = ImageTextures;
	LoadMeshData.GraphicsQueueFamilyIndex = GraphicsQueueFamilyIndex;

	u32 MeshCount = LoadMSHFile(&LoadMeshData, &MultithreadingQueue, UniformLayout, &DescriptorPool);

	vk_buffer ScreenRectVertexBuffer = VulkanCreateAndSendBuffer(Device, PhysicalDevice,
			CommandPool, GraphicsQueues[0], (void*)ScreenRectVertices, sizeof(float) * ArrayCount(ScreenRectVertices),
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

	// NOTE(hugo): Command buffers
	// {
	vk_command_buffer_hierarchy CompositionCmdBufferHierarchy =
		VulkanCreateCommandBufferHierarchy(Device, CommandPool, MeshCount);

	// }

	// NOTE(hugo): Semaphore Creation
	// {
	VkSemaphoreCreateInfo SemaphoreCreateInfo = {};
	SemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkSemaphore ImageAvailableSemaphore = {};
	VkSemaphore CompositeCompFinishedSemaphore = {};
	VkSemaphore RenderFinishedSemaphore = {};

	VK_CHECK(vkCreateSemaphore(Device, &SemaphoreCreateInfo, 0, &ImageAvailableSemaphore));
	VK_CHECK(vkCreateSemaphore(Device, &SemaphoreCreateInfo, 0, &CompositeCompFinishedSemaphore));
	VK_CHECK(vkCreateSemaphore(Device, &SemaphoreCreateInfo, 0, &RenderFinishedSemaphore));
	// }

	// NOTE(hugo): Fence Creation
	// {
	VkFenceCreateInfo FenceCreateInfo = {};
	FenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	FenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	u32 FenceCount = ImageCount;
	VkFence* WaitFences = AllocateArray(VkFence, FenceCount);
	for(u32 FenceIndex = 0; FenceIndex < FenceCount; ++FenceIndex)
	{
		VK_CHECK(vkCreateFence(Device, &FenceCreateInfo, 0, WaitFences + FenceIndex));
	}
	// }

	// NOTE(hugo): Shadow Mapping Descriptor Set Layout
	// {
	VkDescriptorSetLayout ShadowMappingUniformLayout = {};
	{
		VkDescriptorSetLayoutBinding ShadowMappingUBOLayoutBinding = {};
		ShadowMappingUBOLayoutBinding.binding = 0;
		ShadowMappingUBOLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		ShadowMappingUBOLayoutBinding.descriptorCount = 1;
		ShadowMappingUBOLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		ShadowMappingUBOLayoutBinding.pImmutableSamplers = 0;

		VkDescriptorSetLayoutCreateInfo UniformLayoutCreateInfo = {};
		UniformLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		UniformLayoutCreateInfo.bindingCount = 1;
		UniformLayoutCreateInfo.pBindings = &ShadowMappingUBOLayoutBinding;
		VK_CHECK(vkCreateDescriptorSetLayout(Device, &UniformLayoutCreateInfo, 0, &ShadowMappingUniformLayout));
	}
	// }

	// NOTE(hugo): Shadow Mapping Uniform Buffer creation
	// {
	VkDeviceSize ShadowMappingUniformBufferSize = sizeof(shadow_mapping_ubo);
	vk_buffer ShadowMappingUniformBuffer = VulkanCreateBuffer(Device, PhysicalDevice,
			ShadowMappingUniformBufferSize,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	// }

	// NOTE(hugo): Shadow Mapping Descriptor Sets
	// {
	VkDescriptorSet ShadowMappingDescriptorSet = {};
	VkDescriptorSetLayout ShadowMappingLayouts[] = {ShadowMappingUniformLayout};
	{
		VkDescriptorSetAllocateInfo DescSetAllocInfo = {};
		DescSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		DescSetAllocInfo.descriptorPool = DescriptorPool;
		DescSetAllocInfo.descriptorSetCount = ArrayCount(ShadowMappingLayouts);
		DescSetAllocInfo.pSetLayouts = ShadowMappingLayouts;

		VK_CHECK(vkAllocateDescriptorSets(Device, &DescSetAllocInfo, &ShadowMappingDescriptorSet));

		VkDescriptorBufferInfo DescriptorBufferInfo = {};
		DescriptorBufferInfo.buffer = ShadowMappingUniformBuffer.Buffer;
		DescriptorBufferInfo.offset = 0;
		DescriptorBufferInfo.range = sizeof(shadow_mapping_ubo);

		VkWriteDescriptorSet WriteDescriptorSet = {};
		WriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		WriteDescriptorSet.dstSet = ShadowMappingDescriptorSet;
		WriteDescriptorSet.dstBinding = 0;
		WriteDescriptorSet.dstArrayElement = 0;
		WriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		WriteDescriptorSet.descriptorCount = 1;
		WriteDescriptorSet.pBufferInfo = &DescriptorBufferInfo;
		WriteDescriptorSet.pImageInfo = 0;
		WriteDescriptorSet.pTexelBufferView = 0;
		vkUpdateDescriptorSets(Device, 1, &WriteDescriptorSet, 0, 0);
	}
	// }

	// NOTE(hugo): Tone Mapping Descriptor Set
	// {
	VkDescriptorSetLayout ToneMappingSetLayout = {};
	{
		VkDescriptorSetLayoutBinding SamplerLayoutBinding = {};
		SamplerLayoutBinding.binding = 1; // TODO(hugo): Automatic increment binding index
		SamplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		SamplerLayoutBinding.descriptorCount = 1;
		SamplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		SamplerLayoutBinding.pImmutableSamplers = 0;

		VkDescriptorSetLayoutCreateInfo ToneMappingLayoutCreateInfo = {};
		ToneMappingLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		ToneMappingLayoutCreateInfo.bindingCount = 1;
		ToneMappingLayoutCreateInfo.pBindings = &SamplerLayoutBinding;
		VK_CHECK(vkCreateDescriptorSetLayout(Device, &ToneMappingLayoutCreateInfo, 0, &ToneMappingSetLayout));
	}
	
	VkDescriptorSet ToneMappingDescriptorSet = {};
	{
		VkDescriptorSetAllocateInfo DescSetAllocInfo = {};
		DescSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		DescSetAllocInfo.descriptorPool = DescriptorPool;
		DescSetAllocInfo.descriptorSetCount = 1;
		DescSetAllocInfo.pSetLayouts = &ToneMappingSetLayout;

		VK_CHECK(vkAllocateDescriptorSets(Device, &DescSetAllocInfo, &ToneMappingDescriptorSet));

		VkDescriptorImageInfo DescImageInfo = {};
		DescImageInfo.sampler = ToneMappingSampler;
		DescImageInfo.imageView = CompositePassColorAttachment.ImageView;
		DescImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		VkWriteDescriptorSet WriteDescriptorSet = {};
		WriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		WriteDescriptorSet.dstSet = ToneMappingDescriptorSet;
		WriteDescriptorSet.dstBinding = 1;
		WriteDescriptorSet.dstArrayElement = 0;
		WriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		WriteDescriptorSet.descriptorCount = 1;
		WriteDescriptorSet.pBufferInfo = 0;
		WriteDescriptorSet.pImageInfo = &DescImageInfo;
		WriteDescriptorSet.pTexelBufferView = 0;
		vkUpdateDescriptorSets(Device, 1, &WriteDescriptorSet, 0, 0);
	}
	// }

	// NOTE(hugo): Shadow Mapping Pipeline Layout
	// {
	VkPipelineLayout ShadowMappingPipelineLayout = {};
	{
		VkPipelineLayoutCreateInfo LayoutCreateInfo = {};
		LayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		LayoutCreateInfo.setLayoutCount = 1;
		LayoutCreateInfo.pSetLayouts = &ShadowMappingUniformLayout;
		LayoutCreateInfo.pushConstantRangeCount = 0;
		LayoutCreateInfo.pPushConstantRanges = 0;

		VK_CHECK(vkCreatePipelineLayout(Device, &LayoutCreateInfo, 0, &ShadowMappingPipelineLayout));
	}
	// }

	// NOTE(hugo): Tone Mapping Pipeline Layout
	// {
	VkPipelineLayout ToneMappingPipelineLayout = {};
	{
		VkPipelineLayoutCreateInfo LayoutCreateInfo = {};
		LayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		LayoutCreateInfo.setLayoutCount = 1;
		LayoutCreateInfo.pSetLayouts = &ToneMappingSetLayout;
		LayoutCreateInfo.pushConstantRangeCount = 0;
		LayoutCreateInfo.pPushConstantRanges = 0;

		VK_CHECK(vkCreatePipelineLayout(Device, &LayoutCreateInfo, 0, &ToneMappingPipelineLayout));
	}
	// }

	// NOTE(hugo): Shadow Mapping Shader Stage Creation
	// {
	vk_shader_stage ShadowMappingShaderStage = VulkanCreateShaderStage(Device, "shadowmapping.vert.spv", "shadowmapping.frag.spv");
	// }
	
	vk_shader_stage ToneMappingShaderStage = VulkanCreateShaderStage(Device, "tonemapping.vert.spv", "tonemapping.frag.spv");

	// NOTE(hugo): Shadow Mapping Graphics Pipeline creation
	// {
	VkPipeline ShadowMappingPipeline =
		VulkanCreateShadowMappingPipeline(Device, ShadowMappingShaderStage, 
				ShadowMappingPipelineLayout, ShadowMappingRenderPass);
	// }

	VkPipeline ToneMappingPipeline =
		VulkanCreateToneMappingPipeline(Device, ToneMappingShaderStage,
				ToneMappingPipelineLayout, ToneMappingRenderPass,
				SelectedExtent.width, SelectedExtent.height);

	// NOTE(hugo): Shadow Mapping Command Buffer
	// {
	vk_command_buffer_hierarchy ShadowMappingCmdBufferHierarchy =
		VulkanCreateCommandBufferHierarchy(Device, CommandPool, MeshCount);

	VkSemaphore ShadowMappingSemaphore = {};
	{
		VkSemaphoreCreateInfo SemaphoreCreateInfo = {};
		SemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		VK_CHECK(vkCreateSemaphore(Device, &SemaphoreCreateInfo, 0, &ShadowMappingSemaphore));
	}
	// }

	VkCommandBuffer ToneMappingCommandBuffer = VulkanCreatePrimaryCommandBuffer(Device, CommandPool);

	float TargetSecondsPerFrame = 1.0f / 30.0f;

	game_input Input[2] = {};
	game_input *NewInput = &Input[0];
	game_input *OldInput = &Input[1];

	SDL_ShowWindow(Window);

	game_memory GameMemory = {};
	GameMemory.PersistentStorageSize = Megabytes(512);
	GameMemory.PersistentStorage = malloc(GameMemory.PersistentStorageSize);
	Assert(GameMemory.PersistentStorage);
	memset(GameMemory.PersistentStorage, 0, GameMemory.PersistentStorageSize);

	GameMemory.TransientStorageSize = Gigabytes(2);
	GameMemory.TransientStorage = malloc(GameMemory.TransientStorageSize);
	Assert(GameMemory.TransientStorage);
	memset(GameMemory.TransientStorage, 0, GameMemory.TransientStorageSize);

	u32 LastCounter = SDL_GetTicks();

	while(GlobalRunning)
	{
		// NOTE(hugo) : Input processing
		// {
		NewInput->dtForFrame = TargetSecondsPerFrame;

		game_controller_input* OldKeyboardController = &OldInput->KeyboardController;
		game_controller_input* NewKeyboardController = &NewInput->KeyboardController;
		*NewKeyboardController = {};
		for(u32 ButtonIndex = 0;
				ButtonIndex < ArrayCount(NewKeyboardController->Buttons);
				++ButtonIndex)
		{
			NewKeyboardController->Buttons[ButtonIndex].EndedDown =
				OldKeyboardController->Buttons[ButtonIndex].EndedDown;
		}

		{
			SDLProcessPendingEvents(NewKeyboardController, NewInput);
		}

		{
			SDL_Point MouseP;
			Uint32 MouseState = SDL_GetMouseState(&MouseP.x, &MouseP.y);
			f32 MouseX = (r32)MouseP.x;
			f32 MouseY = (r32)MouseP.y;

			NewInput->MouseX = MouseX;
			NewInput->MouseY = MouseY;
			NewInput->MouseZ = 0; // TODO(casey): Support mousewheel?
			Uint32 SDLButtonID[PlatformMouseButton_Count] =
			{
				SDL_BUTTON_LMASK,
				SDL_BUTTON_MMASK,
				SDL_BUTTON_RMASK,
				SDL_BUTTON_X1MASK,
				SDL_BUTTON_X2MASK,
			};
			for(u32 ButtonIndex = 0;
				ButtonIndex < PlatformMouseButton_Count;
				++ButtonIndex)
			{
				NewInput->MouseButtons[ButtonIndex] = OldInput->MouseButtons[ButtonIndex];
				NewInput->MouseButtons[ButtonIndex].HalfTransitionCount = 0;
				SDLProcessKeyboardEvent(&NewInput->MouseButtons[ButtonIndex],
										(MouseState & SDLButtonID[ButtonIndex]) != 0);
			}
		}

		// }

		GameUpdateAndRender(&GameMemory, NewInput,
				Device, UniformBuffer, ShadowMappingUniformBuffer);

		u32 ImageIndex = 0;
		VK_CHECK(vkAcquireNextImageKHR(Device, Swapchain, MAX_U64,
				ImageAvailableSemaphore, VK_NULL_HANDLE, &ImageIndex));

		// NOTE(hugo): Update the Command Buffers for Shadow Mapping Pass
		{
			VkCommandBufferBeginInfo BeginInfo = {};
			BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			BeginInfo.flags = 0;
			BeginInfo.pInheritanceInfo = 0;
			VK_CHECK(vkBeginCommandBuffer(ShadowMappingCmdBufferHierarchy.Primary, &BeginInfo));

			local_persist float DepthBiasConstant = 1.25f;
			local_persist float DepthBiasSlope = 1.75f;

			VkClearValue ClearValue = {};
			ClearValue.depthStencil = {1.0f, 0};

			VkRenderPassBeginInfo RenderPassBeginInfo = {};
			RenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			RenderPassBeginInfo.renderPass = ShadowMappingRenderPass;
			RenderPassBeginInfo.framebuffer = ShadowMappingFramebuffer;
			RenderPassBeginInfo.renderArea.offset = {0, 0};
			RenderPassBeginInfo.renderArea.extent.width = SHADOWMAP_DIM;
			RenderPassBeginInfo.renderArea.extent.height = SHADOWMAP_DIM;
			RenderPassBeginInfo.clearValueCount = 1;
			RenderPassBeginInfo.pClearValues = &ClearValue;

			vkCmdBeginRenderPass(ShadowMappingCmdBufferHierarchy.Primary, &RenderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

			VkCommandBufferInheritanceInfo InheritanceInfo = {};
			InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
			InheritanceInfo.pNext = 0;
			InheritanceInfo.renderPass = ShadowMappingRenderPass;
			InheritanceInfo.subpass = 0;
			InheritanceInfo.framebuffer = ShadowMappingFramebuffer;
			InheritanceInfo.occlusionQueryEnable = VK_FALSE;

			// NOTE(hugo): Update Secondary Command Buffers
			for(u32 CommandBufferIndex = 0; CommandBufferIndex < MeshList.Count; ++CommandBufferIndex)
			{
				VkCommandBuffer CommandBuffer = ShadowMappingCmdBufferHierarchy.Secondary[CommandBufferIndex];
				VkCommandBufferBeginInfo BeginInfo = {};
				BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
				BeginInfo.pInheritanceInfo = &InheritanceInfo;

				VK_CHECK(vkBeginCommandBuffer(CommandBuffer, &BeginInfo));

				vkCmdSetDepthBias(CommandBuffer, DepthBiasConstant, 0.0f, DepthBiasSlope);

				VkBuffer VertexBuffers[] = {MeshBuffers[CommandBufferIndex].Vertex.Buffer};
				VkDeviceSize Offsets[] = {0};
				vkCmdBindPipeline(CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ShadowMappingPipeline);
				vkCmdBindVertexBuffers(CommandBuffer, 0, ArrayCount(VertexBuffers), VertexBuffers, Offsets);
				vkCmdBindIndexBuffer(CommandBuffer, MeshBuffers[CommandBufferIndex].Index.Buffer, 0, VK_INDEX_TYPE_UINT32);
				vkCmdBindDescriptorSets(CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ShadowMappingPipelineLayout, 0, 1, &ShadowMappingDescriptorSet, 0, 0);
				vkCmdDrawIndexed(CommandBuffer, MeshBuffers[CommandBufferIndex].IndexCount, 1, 0, 0, 0);

				VK_CHECK(vkEndCommandBuffer(CommandBuffer));
			}

			vkCmdExecuteCommands(ShadowMappingCmdBufferHierarchy.Primary, MeshList.Count, ShadowMappingCmdBufferHierarchy.Secondary);
			vkCmdEndRenderPass(ShadowMappingCmdBufferHierarchy.Primary);
			VK_CHECK(vkEndCommandBuffer(ShadowMappingCmdBufferHierarchy.Primary));
		}

		// NOTE(hugo): Update the Command Buffers for Composition Pass
		{
			VkCommandBufferBeginInfo BeginInfo = {};
			BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			//BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
			BeginInfo.flags = 0;
			BeginInfo.pInheritanceInfo = 0;
			VK_CHECK(vkBeginCommandBuffer(CompositionCmdBufferHierarchy.Primary, &BeginInfo));

			VkClearValue ClearValues[2] = {};
			ClearValues[0].color = { {0.0f, 0.0f, 0.0f, 1.0f} };
			ClearValues[1].depthStencil = {1.0f, 0};

			VkRenderPassBeginInfo RenderPassBeginInfo = {};
			RenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			RenderPassBeginInfo.renderPass = CompositeRenderPass;
			//RenderPassBeginInfo.framebuffer = SwapchainFramebuffers[ImageIndex]; // TODO(hugo): 0 index here ?
			RenderPassBeginInfo.framebuffer = CompositePassFramebuffer;
			RenderPassBeginInfo.renderArea.offset = {0, 0};
			RenderPassBeginInfo.renderArea.extent.width = SelectedExtent.width;
			RenderPassBeginInfo.renderArea.extent.height = SelectedExtent.height;
			RenderPassBeginInfo.clearValueCount = 2;
			RenderPassBeginInfo.pClearValues = ClearValues;

			vkCmdBeginRenderPass(CompositionCmdBufferHierarchy.Primary, &RenderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

			VkCommandBufferInheritanceInfo InheritanceInfo = {};
			InheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
			InheritanceInfo.pNext = 0;
			InheritanceInfo.renderPass = CompositeRenderPass;
			InheritanceInfo.subpass = 0;
			//InheritanceInfo.framebuffer = SwapchainFramebuffers[ImageIndex];
			InheritanceInfo.framebuffer = CompositePassFramebuffer;
			InheritanceInfo.occlusionQueryEnable = VK_FALSE;

			// NOTE(hugo): Update Secondary Command Buffers
			for(u32 CommandBufferIndex = 0; CommandBufferIndex < MeshList.Count; ++CommandBufferIndex)
			{
				VkCommandBuffer CommandBuffer = CompositionCmdBufferHierarchy.Secondary[CommandBufferIndex];
				VkCommandBufferBeginInfo BeginInfo = {};
				BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
				BeginInfo.pInheritanceInfo = &InheritanceInfo;

				VK_CHECK(vkBeginCommandBuffer(CommandBuffer, &BeginInfo));

				VkBuffer VertexBuffers[] = {MeshBuffers[CommandBufferIndex].Vertex.Buffer};
				VkDeviceSize Offsets[] = {0};
				vkCmdBindPipeline(CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, CompositeGraphicsPipeline);
				vkCmdBindVertexBuffers(CommandBuffer, 0, ArrayCount(VertexBuffers), VertexBuffers, Offsets);
				vkCmdBindIndexBuffer(CommandBuffer, MeshBuffers[CommandBufferIndex].Index.Buffer, 0, VK_INDEX_TYPE_UINT32);
				vkCmdBindDescriptorSets(CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, CompositePipelineLayout, 0, 1, &DescriptorSets[CommandBufferIndex], 0, 0);
				vkCmdDrawIndexed(CommandBuffer, MeshBuffers[CommandBufferIndex].IndexCount, 1, 0, 0, 0);

				VK_CHECK(vkEndCommandBuffer(CommandBuffer));
			}

			vkCmdExecuteCommands(CompositionCmdBufferHierarchy.Primary, MeshList.Count, CompositionCmdBufferHierarchy.Secondary);
			vkCmdEndRenderPass(CompositionCmdBufferHierarchy.Primary);
			VK_CHECK(vkEndCommandBuffer(CompositionCmdBufferHierarchy.Primary));
		}

		// NOTE(hugo): Update the command buffer for ToneMapping Pass
		{
			VkCommandBufferBeginInfo BeginInfo = {};
			BeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			BeginInfo.flags = 0;
			BeginInfo.pInheritanceInfo = 0;
			VK_CHECK(vkBeginCommandBuffer(ToneMappingCommandBuffer, &BeginInfo));

			VkClearValue ClearValues[2] = {};
			ClearValues[0].color = { {0.0f, 0.0f, 0.0f, 1.0f} };
			ClearValues[1].depthStencil = {1.0f, 0};

			VkRenderPassBeginInfo RenderPassBeginInfo = {};
			RenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			RenderPassBeginInfo.renderPass = ToneMappingRenderPass;
			RenderPassBeginInfo.framebuffer = SwapchainFramebuffers[ImageIndex]; // TODO(hugo): 0 index here ?
			RenderPassBeginInfo.renderArea.offset = {0, 0};
			RenderPassBeginInfo.renderArea.extent.width = SelectedExtent.width;
			RenderPassBeginInfo.renderArea.extent.height = SelectedExtent.height;
			RenderPassBeginInfo.clearValueCount = 2;
			RenderPassBeginInfo.pClearValues = ClearValues;

			vkCmdBeginRenderPass(ToneMappingCommandBuffer, &RenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

			VkBuffer VertexBuffers[] = {ScreenRectVertexBuffer.Buffer};
			VkDeviceSize Offsets[] = {0};

			vkCmdBindPipeline(ToneMappingCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ToneMappingPipeline);
			vkCmdBindVertexBuffers(ToneMappingCommandBuffer, 0, ArrayCount(VertexBuffers), VertexBuffers, Offsets);
			//vkCmdBindIndexBuffer(ToneMappingCommandBuffer, ScreenRectIndexBuffer, 0, VK_INDEX_TYPE_UINT32);
			vkCmdBindDescriptorSets(ToneMappingCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ToneMappingPipelineLayout, 0, 1, &ToneMappingDescriptorSet, 0, 0);
			vkCmdDraw(ToneMappingCommandBuffer, 6, 1, 0, 0);

			vkCmdEndRenderPass(ToneMappingCommandBuffer);
			VK_CHECK(vkEndCommandBuffer(ToneMappingCommandBuffer));
		}

		// NOTE(hugo): Perform the rendering on the GPU
		{
			// NOTE(hugo): Shadow Mapping commands submitting
			{
				VkPipelineStageFlags WaitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
				VkSubmitInfo SubmitInfo = {};
				SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				SubmitInfo.waitSemaphoreCount = 0;
				SubmitInfo.pWaitSemaphores = 0;
				SubmitInfo.pWaitDstStageMask = WaitStages;
				SubmitInfo.commandBufferCount = 1;
				SubmitInfo.pCommandBuffers = &ShadowMappingCmdBufferHierarchy.Primary;
				SubmitInfo.signalSemaphoreCount = 1;
				SubmitInfo.pSignalSemaphores = &ShadowMappingSemaphore;

				VK_CHECK(vkQueueSubmit(GraphicsQueues[0], 1, &SubmitInfo, 0));
			}

			// NOTE(hugo): Composite comp. commands submitting
			{
				VkSemaphore WaitSemaphores[] = {ShadowMappingSemaphore};
				//VkSemaphore WaitSemaphores[] = {ImageAvailableSemaphore, ShadowMappingSemaphore};
				VkPipelineStageFlags WaitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
				VkSemaphore SignalSemaphores[] = {CompositeCompFinishedSemaphore};
				VkSubmitInfo SubmitInfo = {};
				SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				SubmitInfo.waitSemaphoreCount = ArrayCount(WaitSemaphores);
				SubmitInfo.pWaitSemaphores = WaitSemaphores;
				SubmitInfo.pWaitDstStageMask = WaitStages;
				SubmitInfo.commandBufferCount = 1;
				SubmitInfo.pCommandBuffers = &CompositionCmdBufferHierarchy.Primary;
				SubmitInfo.signalSemaphoreCount = ArrayCount(SignalSemaphores);
				SubmitInfo.pSignalSemaphores = SignalSemaphores;

				VK_CHECK(vkQueueSubmit(GraphicsQueues[0], 1, &SubmitInfo, 0));
				//VK_CHECK(vkQueueSubmit(GraphicsQueues[0], 1, &SubmitInfo, WaitFences[ImageIndex]));
			}

			// NOTE(hugo): Do we really need the fence here ?
			VK_CHECK(vkWaitForFences(Device, 1, WaitFences + ImageIndex, VK_TRUE, MAX_U64));
			VK_CHECK(vkResetFences(Device, 1, WaitFences + ImageIndex));

			VkSemaphore SignalSemaphores[] = {RenderFinishedSemaphore};
			//VkSemaphore SignalSemaphores[] = {CompositeCompFinishedSemaphore};

			// NOTE(hugo): Tone Mapping commands submitting
			{
				VkSemaphore WaitSemaphores[] = {ImageAvailableSemaphore, CompositeCompFinishedSemaphore};
				VkPipelineStageFlags WaitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
				VkSubmitInfo SubmitInfo = {};
				SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				SubmitInfo.waitSemaphoreCount = ArrayCount(WaitSemaphores);
				SubmitInfo.pWaitSemaphores = WaitSemaphores;
				SubmitInfo.pWaitDstStageMask = WaitStages;
				SubmitInfo.commandBufferCount = 1;
				SubmitInfo.pCommandBuffers = &ToneMappingCommandBuffer;
				SubmitInfo.signalSemaphoreCount = ArrayCount(SignalSemaphores);
				SubmitInfo.pSignalSemaphores = SignalSemaphores;

				VK_CHECK(vkQueueSubmit(GraphicsQueues[0], 1, &SubmitInfo, WaitFences[ImageIndex]));
			}

			VkSwapchainKHR Swapchains[] = {Swapchain};

			VkPresentInfoKHR PresentInfo = {};
			PresentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			PresentInfo.waitSemaphoreCount = ArrayCount(SignalSemaphores);
			PresentInfo.pWaitSemaphores = SignalSemaphores;
			PresentInfo.swapchainCount = ArrayCount(Swapchains);
			PresentInfo.pSwapchains = Swapchains;
			PresentInfo.pImageIndices = &ImageIndex;
			PresentInfo.pResults = 0;

			VK_CHECK(vkQueuePresentKHR(PresentationQueue, &PresentInfo));
			VK_CHECK(vkQueueWaitIdle(PresentationQueue));
		}

		game_input *Temp = NewInput;
		NewInput = OldInput;
		OldInput = Temp;

		// NOTE(hugo): We have a constant 30 FPS
		float WorkCounter = SDL_GetTicks();
		float WorkSecondsElapsed = ((float)(WorkCounter - LastCounter)) / 1000.0f;

		{
			char WindowNewTitleBuffer[128];
			snprintf(WindowNewTitleBuffer, sizeof(WindowNewTitleBuffer),
					"SDL Vulkan Demo by Hugo Viala - %dms - %i FPS",
					(int)(WorkSecondsElapsed * 1000.0f),
					(int)(1.0f / WorkSecondsElapsed));
			SDL_SetWindowTitle(Window, WindowNewTitleBuffer);
		}

		float SecondsElapsedForFrame = WorkSecondsElapsed;
		if(SecondsElapsedForFrame < TargetSecondsPerFrame)
		{
			u32 SleepMS = (u32)(1000.0f * (TargetSecondsPerFrame - SecondsElapsedForFrame));
			if(SleepMS > 0)
			{
				SDL_Delay(SleepMS);
			}
		}
		float EndWorkCounter = SDL_GetTicks();
		LastCounter = EndWorkCounter;
	}

	vkDeviceWaitIdle(Device);

	// NOTE(hugo): Destroying everything about the Shadow Mapping
	// {
	// TODO(hugo): Free the primary and secondary command buffers for shadow mapping
#if 0
	vkFreeCommandBuffers(Device, CommandPool, ImageCount, ShadowMappingCommandBuffers);
	Free(ShadowMappingCommandBuffers);
#endif

	VulkanDestroyBuffer(Device, ShadowMappingUniformBuffer);
	vkDestroyShaderModule(Device, ShadowMappingShaderStage.VertexModule, 0);
	vkDestroyShaderModule(Device, ShadowMappingShaderStage.FragmentModule, 0);

	vkDestroySampler(Device, ShadowMappingSampler, 0);
	vkDestroyFramebuffer(Device, ShadowMappingFramebuffer, 0);

	vkDestroyDescriptorSetLayout(Device, ShadowMappingUniformLayout, 0);

	VulkanDestroyAttachment2D(Device, ShadowMappingAttachment);
	vkDestroyRenderPass(Device, ShadowMappingRenderPass, 0);

	vkDestroyPipeline(Device, ShadowMappingPipeline, 0);
	vkDestroyPipelineLayout(Device, ShadowMappingPipelineLayout, 0);

	// }

	// TODO(hugo): Free the primary and secondary command buffers for composition
#if 0
	vkFreeCommandBuffers(Device, CommandPool, ImageCount, CommandBuffers);
	Free(CommandBuffers);
#endif

	Free(GraphicsQueues);

	for(u32 FenceIndex = 0; FenceIndex < FenceCount; ++FenceIndex)
	{
		vkDestroyFence(Device, WaitFences[FenceIndex], 0);
	}
	Free(WaitFences);

	vkDestroySemaphore(Device, CompositeCompFinishedSemaphore, 0);
	vkDestroySemaphore(Device, ImageAvailableSemaphore, 0);
	vkDestroySemaphore(Device, RenderFinishedSemaphore, 0);
	vkDestroySemaphore(Device, ShadowMappingSemaphore, 0);

	vkDestroyPipeline(Device, ToneMappingPipeline, 0);
	vkDestroyRenderPass(Device, ToneMappingRenderPass, 0);
	vkDestroyPipelineLayout(Device, ToneMappingPipelineLayout, 0);
	vkDestroyDescriptorSetLayout(Device, ToneMappingSetLayout, 0);
	vkDestroyShaderModule(Device, ToneMappingShaderStage.VertexModule, 0);
	vkDestroyShaderModule(Device, ToneMappingShaderStage.FragmentModule, 0);
	vkDestroySampler(Device, ToneMappingSampler, 0);

	vkDestroySampler(Device, TextureSampler, 0);

	for(u32 TextureIndex = 0; TextureIndex < TextureList.Count; ++TextureIndex)
	{
		vk_attachment* Image = ImageTextures + TextureIndex;
		if(Image->ImageView != VK_NULL_HANDLE)
		{
			// TODO(hugo): Assert here that these are not VK_NULL_HANDLE
			// instead of having an if maybe
			vkDestroyImageView(Device, Image->ImageView, 0);
			vkDestroyImage(Device, Image->Image, 0);
			vkFreeMemory(Device, Image->Memory, 0);
		}
	}

	//vkDestroyImageView(Device, DepthImageView, 0);
	//vkDestroyImage(Device, DepthImage, 0);
	//vkFreeMemory(Device, DepthImageMemory, 0);
	VulkanDestroyBuffer(Device, UniformBuffer);

	for(u32 MeshIndex = 0; MeshIndex < MeshList.Count; ++MeshIndex)
	{
		VulkanDestroyBuffer(Device, MeshBuffers[MeshIndex].Index);
		VulkanDestroyBuffer(Device, MeshBuffers[MeshIndex].Vertex);
	}

	vkDestroyCommandPool(Device, CommandPool, 0);

	for(u32 FramebufferIndex = 0; FramebufferIndex < ImageCount; ++FramebufferIndex)
	{
		vkDestroyFramebuffer(Device, SwapchainFramebuffers[FramebufferIndex], 0);
	}
	Free(SwapchainFramebuffers);

	vkDestroyPipeline(Device, CompositeGraphicsPipeline, 0);
	vkDestroyPipelineLayout(Device, CompositePipelineLayout, 0);
	vkDestroyRenderPass(Device, CompositeRenderPass, 0);
	vkDestroyFramebuffer(Device, CompositePassFramebuffer, 0);

	VulkanDestroyAttachment2D(Device, CompositePassColorAttachment);
	VulkanDestroyAttachment2D(Device, CompositePassDepthAttachment);

	vkDestroyShaderModule(Device, ShaderStage.VertexModule, 0);
	vkDestroyShaderModule(Device, ShaderStage.FragmentModule, 0);

	vkDestroyDescriptorPool(Device, DescriptorPool, 0);

	vkDestroyDescriptorSetLayout(Device, UniformLayout, 0);

	VulkanDestroyBuffer(Device, ScreenRectVertexBuffer);

	for(u32 ImageViewIndex = 0; ImageViewIndex < ImageCount; ++ImageViewIndex)
	{
		vkDestroyImageView(Device, SwapchainImageViews[ImageViewIndex], 0);
	}
	Free(SwapchainImageViews);
	vkDestroySwapchainKHR(Device, Swapchain, 0);
	vkDestroyDevice(Device, 0);
#if VALIDATION_LAYER
	DestroyDebugReport(Instance, DebugReportCallback, 0);
#endif
	vkDestroyInstance(Instance, 0);
	SDL_DestroyWindow(Window);

	SDL_Quit();

	return(0);
}
